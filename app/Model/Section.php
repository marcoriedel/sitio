<?php
class Section extends AppModel {
var $name = 'Section';

/*public $belongsTo = array(
    'OtherModel' => array(
        'className' => 'OtherModel'
    )
);*/

public $hasMany = array(
'Article' => array('className' => 'Article'));

public $actsAs = array(
    'UploadPack.Upload' => array(
        'image1' => array(
            'styles' => array(
                'thumb' => '[80x80]',
                'slide' => '[1440x520]'
            )
        ),
        'image2' => array(
            'styles' => array(
                'thumb' => '[80x80]',
                'slide' => '[1440x520]'
            )
        ),
        'image3' => array(
            'styles' => array(
                'thumb' => '[80x80]',
                'slide' => '[1440x520]'
            )
        ),
        'image4' => array(
            'styles' => array(
                'thumb' => '[80x80]',
                'slide' => '[1440x520]'
            )
        ),
        'image5' => array(
            'styles' => array(
                'thumb' => '[80x80]',
                'slide' => '[1440x520]'
            )
        )
    )
);

/*public $actsAs = array(
    'UploadPack.Upload' => array(
        'image' => array(
            'styles' => array(
                'thumb' => '[80x80]'
            )
        )
    )
);*/

/*public $validate = array(
'attr' => array(
        'rule' => 'notEmpty',
        'message' => 'el attr no puede estar vacio',
        'rule' => 'isUnique',
        'message' => 'el attr ya existe',
    )   
);*/
}
?>
