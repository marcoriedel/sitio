<?php
class Doctor extends AppModel {
var $name = 'Doctor';

public $virtualFields = array(
    'name' => 'CONCAT(Doctor.firstName, " ", Doctor.lastName)'
);

public $displayField = 'name';

public $belongsTo = array(
'User' => array('className' => 'User'));

public $hasMany = array(
'Turnrule' => array('className' => 'Turnrule'),
'Turn' => array('className' => 'Turn'),
'Holiday' => array('className' => 'Holiday'));

var $hasAndBelongsToMany = array(
        'Obrasocial' => array(
            'className' => 'Obrasocial',
            'joinTable' => 'doctors_obrasocials',
            'foreignKey' => 'doctor_id',
            'associationForeignKey' => 'obrasocial_id'
        ),
    );   

public $validate = array(
'firstName' => array(
        'rule' => 'notEmpty',
        'message' => 'El nombre no puede estar vacio'        
    ),
'lastName' => array(
        'rule' => 'notEmpty',
        'message' => 'El nombre no puede estar vacio'        
    ),
'email' => array(
        'rule' => 'notEmpty',
        'message' => 'El email no puede estar vacio',
        'rule' => 'isUnique',
        'message' => 'El Email ya existe',        
    )
);
}
?>
