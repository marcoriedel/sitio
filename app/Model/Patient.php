<?php
class Patient extends AppModel {
var $name = 'Patient';

public $virtualFields = array(
    'name' => 'CONCAT(Patient.firstName, " ", Patient.lastName)'
);

public $displayField = 'name';

public $belongsTo = array(
'User' => array('className' => 'User')
,'Obrasocial' => array('className' => 'Obrasocial')
);




/*public $belongsTo = array(
    'OtherModel' => array(
        'className' => 'OtherModel'
    )
);*/

public $hasMany = array(
    'Turn' => array(
        'className' => 'Turn'
    )
    ,'Review' => array(
    'className' => 'Review')
);

/*public $actsAs = array(
    'UploadPack.Upload' => array(
        'image' => array(
            'styles' => array(
                'thumb' => '[80x80]'
            )
        )
    )
);*/

public $validate = array(
'firstName' => array(
        'rule' => 'notEmpty',
        'message' => 'El nombre no puede estar vacio'        
    ),
'lastName' => array(
        'rule' => 'notEmpty',
        'message' => 'El nombre no puede estar vacio'        
    ),
'email' => array(
        'rule' => 'notEmpty',
        'message' => 'El email no puede estar vacio',
              
    ),
'phone'=>array(
        'rule' => 'numeric',
        'allowEmpty' => true, 
        'message'=>'Telefono debe ser numerico',
    ),

);
}
?>
