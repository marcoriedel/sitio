<?php

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {
var $name = 'User';

public $virtualFields = array(
    'name' => 'User.username'
);

public $displayField = 'name';


public $belongsTo = array(
'Usertype' => array('className' => 'Usertype'));

public $hasMany = array(
'Doctor' => array('className' => 'Doctor'),'Patient' => array('className' => 'Patient'));

public function beforeSave($options = array()) {
    if (isset($this->data[$this->alias]['password'])) {
        $passwordHasher = new BlowfishPasswordHasher();
        $this->data[$this->alias]['password'] = $passwordHasher->hash(
            $this->data[$this->alias]['password']
        );
    }
    return true;
}

/*public $actsAs = array(
    'UploadPack.Upload' => array(
        'image' => array(
            'styles' => array(
                'thumb' => '[80x80]'
            )
        )
    )
);*/

public $validate = array(

'dni' => array(
        'rule-1' => array(
            'rule' => 'isUnique',
        'message' => 'Este DNI ya existe',
         ),
        'rule-2' => array(
            'rule' => array('minLength', 8),
            'message' => 'Minimum length of 8 characters'
        ),
        'rule-3' => array(
            'rule' => 'notEmpty',
         )
    ),
'password' => array(
        'rule' => array('minLength', '8'),
        'message' => 'Mínimo 8 digitos')
);
}
?>
