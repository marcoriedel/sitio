<?php
class Obrasocial extends AppModel {
var $name = 'Obrasocial';
public $displayField = 'name';

public $hasMany = array(
'Patient' => array('className' => 'Patient'));

var $hasAndBelongsToMany = array(
        'Doctor' => array(
            'className' => 'Doctor',
            'joinTable' => 'doctors_obrasocials',
            'foreignKey' => 'obrasocial_id',
            'associationForeignKey' => 'doctor_id'
        ),
    ); 



}

/*public $actsAs = array(
    'UploadPack.Upload' => array(
        'image' => array(
            'styles' => array(
                'thumb' => '[80x80]'
            )
        )
    )
);*/

/*public $validate = array(
'attr' => array(
        'rule' => 'notEmpty',
        'message' => 'el attr no puede estar vacio',
        'rule' => 'isUnique',
        'message' => 'el attr ya existe',
    )   
);

*/

?>
