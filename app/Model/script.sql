CREATE TABLE usertypes(
id int(11) AUTO_INCREMENT PRIMARY KEY,
name varchar(1000)
)
;

CREATE TABLE users(
id int(11) AUTO_INCREMENT PRIMARY KEY,
dni int(8),
password(1000),
active tinyint(1),
userType_id int(11)
)
;

CREATE TABLE doctors(
id int(11) AUTO_INCREMENT PRIMARY KEY,
user_id int(11),
firstName varchar(1000),
lastName varchar(1000),
email varchar(1000)
registration varchar(1000),
specialism varchar(1000)
)
;

CREATE TABLE turnrules(
id int(11) AUTO_INCREMENT PRIMARY KEY,
doctor_id int(11),
day_id int(11),
start int(11),
end int(11)
)
;

CREATE TABLE days(
id int(11) AUTO_INCREMENT PRIMARY KEY,
name varchar(1000)
)
;

CREATE TABLE turns(
id int(11) AUTO_INCREMENT PRIMARY KEY,
doctor_id int(11),
patient_id int(11),
datetime datetime,
confirm tinyint(1),
attended tinyint(1)
)
;

CREATE TABLE patients(
id int(11) AUTO_INCREMENT PRIMARY KEY,
user_id int(11),
firstName varchar(1000),
lastName varchar(1000),
email varchar(1000)
)
;

CREATE TABLE consultorios(
id int(11) AUTO_INCREMENT PRIMARY KEY,
name varchar(1000),
address varchar(1000),
phone varchar(1000),
email varchar(1000)
)
;

CREATE TABLE sectionss(
id int(11) AUTO_INCREMENT PRIMARY KEY,
name varchar(1000)
)
;

CREATE TABLE articless(
id int(11) AUTO_INCREMENT PRIMARY KEY,
section_id int(11),
text varchar(10000)
)
;



ALTER TABLE `users` ADD `password` VARCHAR(256) NOT NULL AFTER `dni`;