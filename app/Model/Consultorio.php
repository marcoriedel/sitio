<?php
class Consultorio extends AppModel {
var $name = 'Consultorio';

/*public $belongsTo = array(
    'OtherModel' => array(
        'className' => 'OtherModel'
    )
);*/

/*public $hasMany = array(
    'AnotherModel' => array(
        'className' => 'AnotherModel'
    )
);*/

/*public $actsAs = array(
    'UploadPack.Upload' => array(
        'image' => array(
            'styles' => array(
                'thumb' => '[80x80]'
            )
        )
    )
);*/

public $validate = array(
'name' => array(
        'rule' => 'notEmpty',
        'message' => 'el name no puede estar vacio',
        'rule' => 'isUnique',
        'message' => 'el name ya existe',
    ),
'address' => array(
        'rule' => 'notEmpty',
        'message' => 'el address no puede estar vacio',
        'rule' => 'isUnique',
        'message' => 'el address ya existe',
    ),
'phone' => array(
        'rule' => 'notEmpty',
        'message' => 'el phone no puede estar vacio',
        'rule' => 'isUnique',
        'message' => 'el phone ya existe',
    ),
'email' => array(
        'rule' => 'notEmpty',
        'message' => 'el email no puede estar vacio',
        'rule' => 'isUnique',
        'message' => 'el email ya existe',
    )
);
}
?>
