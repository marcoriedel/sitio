<?php
class Article extends AppModel {
var $name = 'Article';

public $belongsTo = array(
'Section' => array('className' => 'Section'));

/*public $hasMany = array(
    'AnotherModel' => array(
        'className' => 'AnotherModel'
    )
);*/

public $actsAs = array(
    'UploadPack.Upload' => array(
        'image' => array(
            'styles' => array(
                'index' => '64x64',
                'thumb' => '356x150',
                'page' => '825x550',
            )
        )
    )
);

/*public $validate = array(
'attr' => array(
        'rule' => 'notEmpty',
        'message' => 'el attr no puede estar vacio',
        'rule' => 'isUnique',
        'message' => 'el attr ya existe',
    )   
);*/
}
?>
