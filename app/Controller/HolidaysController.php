<?php
class HolidaysController extends AppController {

public $helpers = array('Html', 'Form');



public function index() {
    $this->set('model',$this->Holiday->find('all'));
}

public function add() {
    if ($this->request->is('post')) {
        $this->Holiday->create();
        if ($this->Holiday->save($this->request->data)) {
            $this->Session->setFlash(__('Holiday ha sido creado!'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops! no se pudo crear Holiday!'),'session_danger_flash');
    }

$doctors = $this->Holiday->Doctor->find('list');
$this->set(compact('doctors'));

}

public function edit($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $Holiday = $this->Holiday->findById($id);
    if (!$Holiday) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Holiday->id = $id;
        if ($this->Holiday->save($this->request->data)) {
            $this->Session->setFlash(__('Holiday ha sido modificada'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Holiday'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $Holiday;
    }
$doctors = $this->Holiday->Doctor->find('list');
$this->set(compact('doctors'));

}

public function delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Holiday->delete($id)) {
        $this->Session->setFlash(
            __('Holiday ha sido eliminado'),'session_success_flash'
        );
        return $this->redirect(array('action' => 'index'));
    }
}

}
?>
