<?php
class UsersController extends AppController {

public $helpers = array('Html', 'Form');
var $uses = array('User');
    
    
public function beforeFilter() {
    parent::beforeFilter();
    // Allow users to register and logout.
    $this->Auth->allow('login', 'logout','add');
}

public function login()
{
    if ($this->request->is('post')) {
    //Tomo el usuario a traves del Username, para validar si esta activo, sin loguearme
    $username = $this->request->data['User']['username'];
    $user = $this->User->find('first', array(
        'conditions' => array('User.username' => $username)
    ));
        if($user != null && $user['User']['active'] != 1)
        {
            $this->Session->setFlash(__('Usuario pendiente de activación!'),'session_warning_flash');
        }
        else if ($this->Auth->login()) {
            switch ($user['User']['usertype_id']) {
                case 1: $controller = 'Admin';
                        $action = 'index';
                    break;
                case 2: $controller = 'Adminsecretary';
                        $action = 'index';
                    break;
                case 4: $controller = 'Admindoctor';
                        $action = 'my_schedule';
                    break;
                default:  $this->Auth->logout();
                    $this->Session->setFlash(__('Acceso restringido'),'session_danger_flash');
                    return $this->redirect(array('controller' => 'front', 'action' => 'index'));
                    break;
            }
            return $this->redirect(array('controller' => $controller, 'action' => $action));
        }
        else
        {
            $this->Session->setFlash(__('Nombre de usuario y/o contrase&ntilde;a incorrectos'),'session_danger_flash');
        }
    }
}
public function logout() {
    $this->Auth->logout();
    return $this->redirect(array('controller' => 'front', 'action' => 'index'));
}    
public function index() {
    $this->set('model',$this->User->find('all'));
}

public function add() {
    if ($this->request->is('post')) {
        $this->User->create();
        if ($this->User->save($this->request->data)) {
            $this->Session->setFlash(__('User ha sido creado!'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops! no se pudo crear User!'),'session_danger_flash');
    }
$usertypes = $this->User->Usertype->find('list');
$this->set(compact('usertypes'));
}

public function edit($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $User = $this->User->findById($id);
    
    if (!$User) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->User->id = $id;
        if ($this->User->save($this->request->data)) {
            $this->Session->setFlash(__('User ha sido modificada'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar User'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $User;
    }
$usertypes = $this->User->Usertype->find('list');
$this->set(compact('usertypes'));

}

public function delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->User->delete($id)) {
        $this->Session->setFlash(
            __('User ha sido eliminado'),'session_success_flash'
        );
        return $this->redirect(array('action' => 'index'));
    }
}

}
?>
