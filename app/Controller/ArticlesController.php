<?php
class ArticlesController extends AppController {

public $helpers = array('Html', 'Form', 'Wysiwyg.Wysiwyg', 'UploadPack.Upload');

public function index() {
    $this->set('model',$this->Article->find('all'));
}

public function add() {

    //el section id prestablecido para la carga de articulos es la seccion de blog
    $section = $this->Article->Section->find('first',array('conditions'=>array('Section.name'=>'blog'))); 
    $this->set('section',$section);

    if ($this->request->is('post')) {
        $this->Article->create();
        if ($this->Article->save($this->request->data)) {
            $this->Session->setFlash(__('Articulo ha sido creado!'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops! no se pudo crear Articulo!'),'session_danger_flash');
    }

$sections = $this->Article->Section->find('list');
$this->set(compact('sections'));
}

public function edit($id = null) {

    //el section id prestablecido para la carga de articulos es la seccion de blog
    $section = $this->Article->Section->find('first',array('conditions'=>array('Section.name'=>'blog'))); 
    $this->set('section',$section);

    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $Article = $this->Article->findById($id);
    if (!$Article) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Article->id = $id;
        if ($this->Article->save($this->request->data)) {
            $this->Session->setFlash(__('Articulo ha sido modificada'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Articulo'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $Article;
    }
$sections = $this->Article->Section->find('list');
$this->set(compact('sections'));
}

public function delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Article->delete($id)) {
        $this->Session->setFlash(
            __('Articulo ha sido eliminado'),'session_success_flash'
        );
        return $this->redirect(array('action' => 'index'));
    }
}

}
?>
