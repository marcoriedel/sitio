<?php
class AdminsecretaryController extends AppController {

var $uses = array('Turn','Turnrule','Doctor','Patient','Day','Consultorio','User','Usertype');
public $helpers = array('Html', 'Form', 'UploadPack.Upload');

public function beforeFilter() {
   parent::beforeFilter();
    // NOT ALLOW SECRETARY USER TYPE
    if ($this->controllerUser['usertype_id'] != 2)
    {
        $this->Session->setFlash(__('Acceso restringido'),'session_danger_flash');
        return $this->redirect($this->controllerUser['homeUrl']);
    }
}	

public function index() {
    
    $this->sendConfirmationEmail();

	$doctors = $this->Doctor->find('all');
	$this->set('doctors',$doctors);	
    $consultorio = $this->Consultorio->findById(1);
    $this->set('consultorio',$consultorio); 
}

public function doctorSchedule() {
	$doctor_id = $this->request->data['cbo_doctor']; 
	$doctor = $this->Doctor->findById($id);
	$this->set('doctor',$doctor);	
}


public function overTurn(){
    $doctors = $this->Doctor->find('all');
    $this->set('doctors',$doctors); 

    $patients = $this->Patient->find('all',array('order' => array('lastName')));
    $this->set('patients',$patients);
    
}


public function overTurn2(){
    $doctors = $this->Doctor->find('all');
    $this->set('doctors',$doctors); 

    $patients = $this->Patient->find('all',array('order' => array('lastName')));
    $this->set('patients',$patients);
    
}

public function patients(){
    
    $this->set('model',$this->Patient->find('all',array(
    'order' => array('Patient.lastName','Patient.firstName','User.username')
    )));    
}

public function addPatient() {
    if ($this->request->is('post')) {
        $this->User->create();
        $this->Patient->create();
        if ($this->User->save($this->request->data)) {
            
            //Se asigna el User id creado al nuevo paciente
            if ($this->Patient->save($this->request->data)) {
                $this->Patient->updateAll(array('user_id' => $this->User->id), 
                            array('Patient.id' => $this->Patient->id));

                $this->Session->setFlash(__('Paciente ha sido creado!'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
                
            
            }
        }
        $this->Session->setFlash(__('Oops! no se pudo crear Paciente!'),'session_danger_flash');
    }

$usertypes = $this->User->Usertype->find('list', array('conditions'=> array('id'=>3)));
$this->set(compact('usertypes'));
//$obrasocials = $this->Obrasocial->find('list');
//$this->set(compact('obrasocials'));

}

public function editPatient($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }
    
    $Patient = $this->Patient->findById($id);
    if (!$Patient) {
        throw new NotFoundException(__('Invalid post'));
    }
    $CurrentUser = $this->User->findById($Patient['Patient']['user_id']);

    if ($this->request->is(array('post', 'put'))) {
        $this->Patient->id = $id;
        
        if ($this->Patient->save($this->request->data)) {
            $this->User->updateAll(array('username' =>$this->request->data['User']['username'] ), 
                           array('User.id' => $CurrentUser['User']['id']));
            $this->Session->setFlash(__('Paciente ha sido modificada'),'session_success_flash');
            return $this->redirect(array('action' => 'patients'));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Paciente'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $Patient;
    }

$users = $this->Patient->User->find('list',array('conditions' => array('usertype_id' => 3)));
$this->set(compact('users'));

//$obrasocials = $this->Obrasocial->find('list');
//$this->set(compact('obrasocials'));

}

public function turnPatient($id = null){

    	$patient = $this->Patient->find('first',array('conditions'=>array('Patient.id'=>$id)));
		$turns = $this->Turn->find('all',array('conditions'=>array('patient_id'=>$patient['Patient']['id'] )));
		$this->set('model',$turns);	
}

public function cancelTurn($code){
        
    $this->Turn->updateAll(array('Turn.confirm' => false,'Turn.attended' => false//,'Turn.present' => false
                            ),array('Turn.code' => $code));

    $turn = $this->Turn->find('first',array('conditions'=>array('code'=>$code)));

    $this->Session->setFlash(
            __('El Turno ha sido Cancelado.'),'session_success_flash'
        );
        return $this->redirect(array('controller'=>'Adminsecretary','action' => 'turnPatient',$turn['Turn']['patient_id']));

}

public function confirmTurn($code){
        
    $this->Turn->updateAll(array('Turn.confirm' => true), 
                            array('Turn.code' => $code));

    $turn = $this->Turn->find('first',array('conditions'=>array('code'=>$code)));

    $this->Session->setFlash(
            __('El Turno ha sido Confirmado.'),'session_success_flash'
        );
        return $this->redirect(array('controller'=>'Adminsecretary','action' => 'turnPatient',$turn['Turn']['patient_id']));

}

public function addWaitingList($code){
        
    $this->Turn->updateAll(array('Turn.present' => true), 
                            array('Turn.code' => $code));

    $turn = $this->Turn->find('first',array('conditions'=>array('code'=>$code)));

    $this->Session->setFlash(
            __('Paciente agregado a lista de espera.'),'session_success_flash'
        );
        return $this->redirect(array('controller'=>'Adminsecretary','action' => 'turnPatient',$turn['Turn']['patient_id']));

}

public function delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }
    $turnDel = $this->Turn->find('first',array('conditions'=>array('Turn.id'=>$id)));
    if ($this->Turn->delete($id)) {
        $this->Session->setFlash(
            __('El turno ha sido eliminado'),'session_danger_flash'
        );
        return $this->redirect(array('controller'=>'Adminsecretary','action' => 'turnPatient',$turnDel['Turn']['patient_id']));
    }
}

public function sendConfirmationEmail(){

    $consultorio = $this->Consultorio->findById(1);
    //Calcula la diferencia entre el ultimo envio de email y la fecha actual
    $interval = $this->dateDiff($consultorio['Consultorio']['confirmationEmailLastSend']);
        
            if ($interval < 0){ //Si la diferencia es mayor a un dia se envian los emails pendientes de confirmacion.

                $turnsNC = $this->Turn->query("SELECT * FROM turns WHERE confirm is null and datetime > now()");

                foreach ($turnsNC as $turn ) {
                    
                    $turnInterval = $this->dateDiff($turn['turns']['datetime'])  ;

                    if ($turnInterval < 4 &&  $turnInterval > 0){//Se envian los email a los turnos con menos de 72 hs

                        $doctor = $this->Doctor->findById($turn['turns']['doctor_id']);
                        if($doctor){
                            $patient = $this->Patient->findById($turn['turns']['patient_id']);
                            if($patient){

                                //$fecha_actual=date("Y-m-d");
                                //var_dump($patient);
                                
                                $Email = new CakeEmail('gmail');
                                $Email->to($patient['Patient']['email']);
                                $Email->template('confirm','confirm');
                                $Email->emailFormat('html');
                                $Email->viewVars(array( 'username' => $patient['Patient']['name'],
                                                        'doctorName' => $doctor['Doctor']['name'],
                                                        'dateTime' => $turn['turns']['datetime'],
                                                        'code'=> $turn['turns']['code']
                                                      ));
                                $Email->subject('Turno ICM Olavarria pendiente de confirmacion.');
                                $Email->send('');
                            }
                        }
                    }
                }
                $this->Turn->query("UPDATE consultorios SET confirmationEmailLastSend = NOW()");
            }

}

public function dateDiff($date){

    $datetime1 = new DateTime();
    $datetime2 = new DateTime($date);
    $interval = $datetime1->diff($datetime2);
    return $interval->format('%R%a');


}


}

?>