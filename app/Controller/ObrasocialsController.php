<?php
class ObrasocialsController extends AppController {
public $uses    = array('Doctor', 'Obrasocial');

public $helpers = array('Html', 'Form');

public function index() {
    $this->set('model',$this->Obrasocial->find('all'));
}

public function add() {
    if ($this->request->is('post')) {
        $this->Obrasocial->create();
        if ($this->Obrasocial->save($this->request->data)) {
            $this->Session->setFlash(__('Obrasocial ha sido creado!'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops! no se pudo crear Obrasocial!'),'session_danger_flash');
    }
$doctors = $this->Doctor->find('list');
$this->set('doctors',$doctors);

}

public function edit($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $Obrasocial = $this->Obrasocial->findById($id);
    if (!$Obrasocial) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Obrasocial->id = $id;
        if ($this->Obrasocial->save($this->request->data)) {
            $this->Session->setFlash(__('Obrasocial ha sido modificada'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Obrasocial'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $Obrasocial;
    }
$doctors = $this->Doctor->find('list');
$this->set('doctors',$doctors);
}

public function delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Obrasocial->delete($id)) {
        $this->Session->setFlash(
            __('Obrasocial ha sido eliminado'),'session_success_flash'
        );
        return $this->redirect(array('action' => 'index'));
    }
}

}
?>
