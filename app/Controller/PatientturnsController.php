<?php
class PatientturnsController extends AppController {

var $uses = array('Turn','Turnrule','Doctor','Patient','Day');
public $helpers = array('Html', 'Form', 'UploadPack.Upload');

public function beforeFilter() {
   parent::beforeFilter();
   $this->Auth->allow();
}	

public function index() {
	$doctors = $this->Doctor->find('all');
	$this->set('doctors',$doctors);	
}


}

?>