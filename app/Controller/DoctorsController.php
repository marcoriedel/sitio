<?php
class DoctorsController extends AppController {
public $uses    = array('Doctor', 'Obrasocial');

public $helpers = array('Html', 'Form');

public function index() {
    $this->set('model',$this->Doctor->find('all'));
}

public function add() {
    if ($this->request->is('post')) {
        $this->Doctor->create();
        if ($this->Doctor->save($this->request->data)) {
            $this->Session->setFlash(__('Doctor ha sido creado!'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops! no se pudo crear Doctor!'),'session_danger_flash');
    }

$obrasocials = $this->Obrasocial->find('list');
$this->set('obrasocials',$obrasocials);


$users = $this->Doctor->User->find('list');
$this->set(compact('users'));
}

public function edit($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $Doctor = $this->Doctor->findById($id);
    
    if (!$Doctor) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Doctor->id = $id;
        if ($this->Doctor->save($this->request->data)) {
            $this->Session->setFlash(__('Doctor ha sido modificada'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Doctor'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $Doctor;
    }

$obrasocials = $this->Obrasocial->find('list');
$this->set('obrasocials',$obrasocials);

$users = $this->Doctor->User->find('list');
$this->set(compact('users'));
}

public function delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Doctor->delete($id)) {
        $this->Session->setFlash(
            __('Doctor ha sido eliminado'),'session_success_flash'
        );
        return $this->redirect(array('action' => 'index'));
    }
}

public function getWarnings ($doctor_id, $obrasocials_id){    
    $this->layout = 'ajax';
    $reconoceObraSocial = false;
    
    $doctor = $this->Doctor->findById($doctor_id);  
    $obra = $this->Obrasocial->findById($obrasocials_id);    
    foreach ($doctor['Obrasocial'] as $os) {
        if($os['id'] == $obrasocials_id){
            $reconoceObraSocial = true;
        }    
    }

    if($reconoceObraSocial){        
        $this->set('model','OK');
    } else {
        $this->set('model',$doctor['Doctor']['message'] . ' ('.$obra['Obrasocial']['name'].')');
    }
}

public function getTurnsLength ($doctor_id){
    $this->layout = 'ajax';

    $doctor = $this->Doctor->findById($doctor_id);
     
     if ($doctor)
     {              
        return $this->set('model',$doctor['Doctor']['turnTime']);
     }
     $error = array('Mensaje' =>'No existe Doctor');
     return $this->set('model',json_encode($error));  
}


}
?>
