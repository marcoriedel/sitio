<?php
class SectionsController extends AppController {

public $helpers = array('Html', 'Form', 'UploadPack.Upload');

public function index() {
    $this->set('model',$this->Section->find('all'));
}

public function add() {
    if ($this->request->is('post')) {
        $this->Section->create();
        if ($this->Section->save($this->request->data)) {
            $this->Session->setFlash(__('Section ha sido creado!'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops! no se pudo crear Section!'),'session_danger_flash');
    }
}

public function edit($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $Section = $this->Section->findById($id);
    if (!$Section) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Section->id = $id;
        if ($this->Section->save($this->request->data)) {
            $this->Session->setFlash(__('Section ha sido modificada'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Section'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $Section;
    }
}

public function delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Section->delete($id)) {
        $this->Session->setFlash(
            __('Section ha sido eliminado'),'session_success_flash'
        );
        return $this->redirect(array('action' => 'index'));
    }
}

}
?>
