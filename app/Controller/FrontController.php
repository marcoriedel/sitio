<?php
class FrontController extends AppController {

var $uses = array('Section','Consultorio','Article');
public $helpers = array('Html', 'Form', 'UploadPack.Upload');

public function beforeFilter() {
   parent::beforeFilter();
   $this->Auth->allow();
   $this->layout = 'front';
   $this->set('consultorio',$this->Consultorio->find('first'));
}	

public function index() {
	$this->set('section',$this->Section->find('first',array('conditions'=>array('Section.name' => 'index'))));
	$this->set('article',$this->Article->find('first'));
}

public function about() {
	$this->set('section',$this->Section->find('first',array('conditions'=>array('Section.name' => 'about'))));
}

public function services() {
	$this->set('section',$this->Section->find('first',array('conditions'=>array('Section.name' => 'services'))));
}

public function blog() {
	$this->set('section',$this->Section->find('first',array('conditions'=>array('Section.name' => 'blog'))));
	$this->set('articles',$this->Article->find('all'));
}

public function article($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

	$this->set('section',$this->Section->find('first',array('conditions'=>array('Section.name' => 'blog'))));
	$this->set('article',$this->Article->findById($id));
}

public function contact() {
	$this->set('section',$this->Section->find('first',array('conditions'=>array('Section.name' => 'contact'))));
}

}

?>