<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
var $uses = array('Usertypes','Patient','Doctor','Obrasocial');
	
public $components = array(
        'Session',
		//'Facebook.Connect' => array('model' => 'User'),
        'Auth' => array(
            'loginRedirect' => array(
                'controller' => 'posts',
                'action' => 'index'
            ),
            'logoutRedirect' => array(
                'controller' => 'pages',
                'action' => 'display',
                'home'
            ),
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish'
                )
            )
        )
    );



public function beforeRender() {
    
    
    }

public function beforeFilter(){        

        /* ENUM PROFILES:
        1 - Administrador
        2 - Secretaria
        3 - Paciente
        4 - Doctor */

        //Objeto que se va a popular con datos del usuario y se va a pasar
        // - A todos los controllers (linea: 116) 
        // - A todas las vistas (linea: 117)        
        $objUser = Array();

        $objUser['isAuth'] = false;

        if($this->Auth->user('id') !== null)
        {
            $objUser['isAuth'] =  true;
            $objUser['usertype_id'] = $this->Auth->user('usertype_id');
            $objUser['userid'] = $this->Auth->user('id');
            $objUser['username'] = $this->Auth->user('username');
            
            switch ($objUser['usertype_id']) {
                case 1:$typeController = 'Admin';
                    break;

                case 2:$typeController = 'Adminsecretary';
                    break;

                case 3:$typeController = 'Adminpatient';
                    $patient = $this->Patient->find('first',array('conditions'=>array('user_id'=>$objUser['userid'])));
                    if($patient)
                    {
                        $objUser['fullname'] = $patient['Patient']['firstName'] . ' ' . $patient['Patient']['lastName'];
                        $objUser['obrasocial'] = $patient['Obrasocial']['name'];
                        $objUser['email'] = $patient['Patient']['email'];
                        $objUser['patient_id'] = $patient['Patient']['id'];
                    }
                    break;

                case 4:$typeController = 'Admindoctor';
                    $doctor = $this->Doctor->find('first',array('conditions'=>array('user_id'=>$objUser['userid'])));
                    if($doctor)
                    {
                        $objUser['fullname'] = $doctor['Doctor']['firstName'] . ' ' . $doctor['Doctor']['lastName'];
                        $objUser['email'] = $doctor['Doctor']['email'];
                        $objUser['doctor_id'] = $doctor['Doctor']['id'];
                    }
                    break;
                
                default:
                    return $this->redirect(array('controller'=>'front','action' => 'index'));    
                    break;
            }

            $objUser['homeUrl'] = array('controller'=>$typeController,'action'=>'index');
            
            $this->controllerUser = $objUser;
            $this->set('viewUser', $objUser);           
        }

        $this->set('isAuthenticated',$objUser['isAuth']);
    }

}
