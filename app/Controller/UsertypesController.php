<?php
class UsertypesController extends AppController {

public $helpers = array('Html', 'Form');

public function index() {
    $this->set('model',$this->Usertype->find('all'));
}

public function add() {
    if ($this->request->is('post')) {
        $this->Usertype->create();
        if ($this->Usertype->save($this->request->data)) {
            $this->Session->setFlash(__('Usertype ha sido creado!'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops! no se pudo crear Usertype!'),'session_danger_flash');
    }
}

public function edit($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $Usertype = $this->Usertype->findById($id);
    if (!$Usertype) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Usertype->id = $id;
        if ($this->Usertype->save($this->request->data)) {
            $this->Session->setFlash(__('Usertype ha sido modificada'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Usertype'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $Usertype;
    }
}

public function delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Usertype->delete($id)) {
        $this->Session->setFlash(
            __('Usertype ha sido eliminado'),'session_success_flash'
        );
        return $this->redirect(array('action' => 'index'));
    }
}

}
?>
