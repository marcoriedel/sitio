<?php
class TurnrulesController extends AppController {

public $helpers = array('Html', 'Form');



public function index() {
    $this->set('model',$this->Turnrule->find('all'));
}

public function add() {
    if ($this->request->is('post')) {
        $this->Turnrule->create();
        if ($this->Turnrule->save($this->request->data)) {
            $this->Session->setFlash(__('Turnrule ha sido creado!'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops! no se pudo crear Turnrule!'),'session_danger_flash');
    }

$doctors = $this->Turnrule->Doctor->find('list');
$this->set(compact('doctors'));
$days = $this->Turnrule->Day->find('list');
$this->set(compact('days'));
}

public function edit($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $Turnrule = $this->Turnrule->findById($id);
    if (!$Turnrule) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Turnrule->id = $id;
        if ($this->Turnrule->save($this->request->data)) {
            $this->Session->setFlash(__('Turnrule ha sido modificada'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Turnrule'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $Turnrule;
    }
$doctors = $this->Turnrule->Doctor->find('list');
$this->set(compact('doctors'));
$days = $this->Turnrule->Day->find('list');
$this->set(compact('days'));
}

public function delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Turnrule->delete($id)) {
        $this->Session->setFlash(
            __('Turnrule ha sido eliminado'),'session_success_flash'
        );
        return $this->redirect(array('action' => 'index'));
    }
}

}
?>
