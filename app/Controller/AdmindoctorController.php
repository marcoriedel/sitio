<?php
class AdmindoctorController extends AppController {

var $uses = array('Turn','Turnrule','Doctor','Patient','Day','Review','Holiday');
public $helpers = array('Html', 'Form', 'UploadPack.Upload');

public function beforeFilter() {
   parent::beforeFilter();	
    // NOT ALLOW DOCTOR USER TYPE
    if ($this->controllerUser['usertype_id'] != 4)
    {
        $this->Session->setFlash(__('Acceso restringido'),'session_danger_flash');
        return $this->redirect($this->controllerUser['homeUrl']);
    }
}	

public function index() {
	$doctors = $this->Doctor->find('all');
	$this->set('doctors',$doctors);	
}

public function my_schedule() {	
}

/* INTERVALOS */


public function myTurnrules() {
    $this->set('model',$this->Turnrule->find('all',array(
        'conditions'=>array('Turnrule.doctor_id' => $this->controllerUser['doctor_id']),
        'order'=>'day_id'
        )));
}

public function add_turnrule() {
    if ($this->request->is('post')) {
        $this->Turnrule->create();
        if ($this->Turnrule->save($this->request->data)) {
            $this->Session->setFlash(__('Turnrule ha sido creado!'),'session_success_flash');
            return $this->redirect(array('action' => 'myTurnrules'));
        }
        $this->Session->setFlash(__('Oops! no se pudo crear Turnrule!'),'session_danger_flash');
    }
$days = $this->Turnrule->Day->find('list');
$this->set(compact('days'));
}

public function edit_turnrule($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $Turnrule = $this->Turnrule->findById($id);
    if (!$Turnrule) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Turnrule->id = $id;
        if ($this->Turnrule->save($this->request->data)) {
            $this->Session->setFlash(__('Turnrule ha sido modificada'),'session_success_flash');
            return $this->redirect(array('action' => 'myTurnrules'));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Turnrule'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $Turnrule;
    }
$days = $this->Turnrule->Day->find('list');
$this->set(compact('days'));
}

public function deleteTurnrule($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Turnrule->delete($id)) {
        $this->Session->setFlash(
            __('Turnrule ha sido eliminado'),'session_success_flash'
        );
        return $this->redirect(array('action' => 'myTurnrules'));
    }
}

public function waitingList(){
    
    $turns = $this->Turn->find('all',
                array('conditions'=> array('doctor_id'=>$this->viewVars['viewUser']['doctor_id'],
                                    'confirm'=>true,
                                    'present'=>true,
                                    'attended'=>false),
                        'order'=>'datetime'
                    ));

    $this->set('turns',$turns);
}
/* Vacaciones*/   

public function myHolidays() {
    $this->set('model',$this->Holiday->find('all',array(
        'conditions'=>array('Holiday.doctor_id' => $this->controllerUser['doctor_id'])
        )));
}

public function add_Holiday() {
    if ($this->request->is('post')) {
        $this->Holiday->create();
        if ($this->Holiday->save($this->request->data)) {
            $this->Session->setFlash(__('El registro ha sido creado!'),'session_success_flash');
            return $this->redirect(array('action' => 'myHolidays'));
        }
        $this->Session->setFlash(__('Oops! no se pudo crear!'),'session_danger_flash');
    }
}

public function edit_Holiday($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $Holiday = $this->Holiday->findById($id);
    if (!$Holiday) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Holiday->id = $id;
        if ($this->Holiday->save($this->request->data)) {
            $this->Session->setFlash(__('El registro ha sido modificada'),'session_success_flash');
            return $this->redirect(array('action' => 'myHolidays'));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Holiday'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $Holiday;
    }

}

public function deleteHoliday($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Holiday->delete($id)) {
        $this->Session->setFlash(
            __('Holiday ha sido eliminado'),'session_success_flash'
        );
        return $this->redirect(array('action' => 'myHolidays'));
    }
}



/*Metodos st para ser usados sin un turno asociado.*/
public function patientReviewst($id){

    $patient = $this->Patient->findById($id);
    $this->set('patient',$patient);

 

    $reviews = $this->Review->find('all', 
                array('conditions' => array('doctor_id'=>$this->viewVars['viewUser']['doctor_id'],
                                            'patient_id'=> $id),
                      'order' =>'review.id desc'));

    $this->set('reviews',$reviews);                                                                                            
    
}

public function addReviewst($id) {
    if ($this->request->is('post')) {
        $this->Review->create();
        
        if ($this->Review->save($this->request->data)) {
            $this->Session->setFlash(__('Entrada creada correctamente!'),'session_success_flash');
            return $this->redirect(array('action' => 'patientReviewst',$id));
        }
        $this->Session->setFlash(__('Oops! no se pudo crear Review!'),'session_danger_flash');
    }

$this->set('patient_id',$id);

$doctors = $this->Doctor->find('list', array('conditions'=>array('id'=>$this->viewVars['viewUser']['doctor_id'])));
$this->set(compact('doctors'));
$patients = $this->Patient->find('list',array('conditions'=>array('id'=>$id)));
$this->set(compact('patients'));

$reviews = $this->Review->find('all', 
                array('conditions' => array('doctor_id'=>$this->viewVars['viewUser']['doctor_id'],
                                            'patient_id'=> $id),
                      'order' =>'review.id desc'));

$this->set('reviews',$reviews);                                                                                            

}

public function editReviewst($id) {
if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $Review = $this->Review->findById($id);

    if (!$Review) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Review->id = $id;
        if ($this->Review->save($this->request->data)) {

            


            $this->Session->setFlash(__('Entrada modificada'),'session_success_flash');
            return $this->redirect(array('action' => 'patientReviewst',$Review['Patient']['id']));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Review'),'session_danger_flash');

        
    }

    if (!$this->request->data) {
        $this->request->data = $Review;
    }

$reviews = $this->Review->find('all', 
                array('conditions' => array('doctor_id'=>$this->viewVars['viewUser']['doctor_id'],
                                            'patient_id'=> $Review['Patient']['id']),
                      'order' =>'review.id desc'));

$this->set('reviews',$reviews);
                                                                                            


}


public function patientReview($id){

    $patient = $this->Patient->findById($id);
    $this->set('patient',$patient);

    $activeTurn = $this->Turn->find('all',
                array('conditions'=> array('doctor_id'=>$this->viewVars['viewUser']['doctor_id'],
                                    'confirm'=>true,
                                    'present'=>true,
                                    'attended'=>false,
                                    'patient_id'=>$id)));

    $reviews = $this->Review->find('all', 
                array('conditions' => array('doctor_id'=>$this->viewVars['viewUser']['doctor_id'],
                                            'patient_id'=> $id),
                      'order' =>'review.id desc'));

    $this->set('reviews',$reviews);                                                                                            
                                                                            
                          
    $this->set('activeTurn',$activeTurn);                                   
}

public function addReview($id) {
    if ($this->request->is('post')) {
        $this->Review->create();
        
        if ($this->Review->save($this->request->data)) {
            $this->Session->setFlash(__('Entrada creada correctamente!'),'session_success_flash');
            return $this->redirect(array('action' => 'patientReview',$id));
        }
        $this->Session->setFlash(__('Oops! no se pudo crear Review!'),'session_danger_flash');
    }

$this->set('patient_id',$id);

$doctors = $this->Doctor->find('list', array('conditions'=>array('id'=>$this->viewVars['viewUser']['doctor_id'])));
$this->set(compact('doctors'));
$patients = $this->Patient->find('list',array('conditions'=>array('id'=>$id)));
$this->set(compact('patients'));

$reviews = $this->Review->find('all', 
                array('conditions' => array('doctor_id'=>$this->viewVars['viewUser']['doctor_id'],
                                            'patient_id'=> $id),
                      'order' =>'review.id desc'));

$this->set('reviews',$reviews);                                                                                            

}

public function editReview($id) {
if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $Review = $this->Review->findById($id);

    if (!$Review) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Review->id = $id;
        if ($this->Review->save($this->request->data)) {
            $this->Session->setFlash(__('Entrada modificada'),'session_success_flash');
            return $this->redirect(array('action' => 'patientReview',$Review['Patient']['id']));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Review'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $Review;
    }

$reviews = $this->Review->find('all', 
                array('conditions' => array('doctor_id'=>$this->viewVars['viewUser']['doctor_id'],
                                            'patient_id'=> $Review['Patient']['id']),
                      'order' =>'review.id desc'));

$this->set('reviews',$reviews);
                                                                                            


}


public function attended($code){
    $this->Turn->updateAll(array('Turn.attended' => true), 
                            array('Turn.code' => $code));

    $turn = $this->Turn->find('first',array('conditions'=>array('code'=>$code)));

    $this->Session->setFlash(
            __('El Turno ha sido completado.'),'session_success_flash'
        );
        return $this->redirect(array('controller'=>'Admindoctor','action' => 'waitingList',$turn['Turn']['doctor_id']));

}

public function patients(){
    
    $this->set('model',$this->Patient->find('all', array(
    'joins' => array(
        array(
            'table' => 'turns',
            'alias' => 'TurnJoin',
            'type' => 'INNER',
            'conditions' => array(
                'TurnJoin.patient_id = Patient.id'
            )
        )
    ),
    'conditions' => array(
        'TurnJoin.doctor_id' => $this->viewVars['viewUser']['doctor_id']
    ),
    'fields' => array('DISTINCT Patient.lastName','Patient.firstName','User.username'),
     'order' => array('Patient.lastName','Patient.firstName','User.username')
)));    
}


}

?>