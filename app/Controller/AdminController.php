<?php
class AdminController extends AppController {

var $uses = array('Turn','Turnrule','Doctor','Patient','Day','Obrasocial');
public $helpers = array('Html', 'Form', 'UploadPack.Upload');

public function beforeFilter() {
   parent::beforeFilter();
    // NOT ALLOW ADMIN USER TYPE
    if ($this->controllerUser['usertype_id'] != 1)
    {
        $this->Session->setFlash(__('Acceso restringido'),'session_danger_flash');
        return $this->redirect($this->controllerUser['homeUrl']);
    }
}	

public function index() {
}

public function doctors() {
    $this->set('model',$this->Doctor->find('all'));
}

public function add_doctor() {
    if ($this->request->is('post')) {
        $this->Doctor->create();
        if ($this->Doctor->saveAssociated($this->request->data)) {
            $this->Session->setFlash(__('El doctor ha sido creado!'),'session_success_flash');
            return $this->redirect(array('controller'=>'admin','action' => 'doctors'));
        }
        $this->Session->setFlash(__('Oops! no se pudo crear Doctor!'),'session_danger_flash');
    }
$obrasocials = $this->Obrasocial->find('list');
$this->set('obrasocials',$obrasocials);

$users = $this->Doctor->User->find('list');
$this->set(compact('users'));
}

public function edit_doctor($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $Doctor = $this->Doctor->findById($id);
    if (!$Doctor) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Doctor->id = $id;
        if ($this->Doctor->save($this->request->data)) {
            $this->Session->setFlash(__('Los datos han sido modificados'),'session_success_flash');
            return $this->redirect(array('controller'=>'admin','action' => 'doctors'));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Doctor'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $Doctor;
    }
$obrasocials = $this->Obrasocial->find('list');
$this->set('obrasocials',$obrasocials);

$users = $this->Doctor->User->find('list');
$this->set(compact('users'));
}

public function secretaries() {
    $this->set('model',$this->User->find('all',array('conditions'=>array('User.usertype_id'=>2))));
}

public function delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Doctor->delete($id)) {
        $this->Session->setFlash(
            __('El Doctor  ha sido eliminado'),'session_success_flash'
        );
        return $this->redirect(array('action' => 'index'));
    }
}

public function add_secretary() {
}

public function edit_secretary() {
}

}

?>