<?php
class ConsultoriosController extends AppController {

public $helpers = array('Html', 'Form');

public function index() {
    $this->set('model',$this->Consultorio->find('all'));
}

public function add() {
    if ($this->request->is('post')) {
        $this->Consultorio->create();
        if ($this->Consultorio->save($this->request->data)) {
            $this->Session->setFlash(__('Consultorio ha sido creado!'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops! no se pudo crear Consultorio!'),'session_danger_flash');
    }
}

public function edit($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $Consultorio = $this->Consultorio->findById($id);
    if (!$Consultorio) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Consultorio->id = $id;
        if ($this->Consultorio->save($this->request->data)) {
            $this->Session->setFlash(__('Datos Actualizados'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Consultorio'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $Consultorio;
    }
}

public function delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Consultorio->delete($id)) {
        $this->Session->setFlash(
            __('Consultorio ha sido eliminado'),'session_success_flash'
        );
        return $this->redirect(array('action' => 'index'));
    }
}

}
?>
