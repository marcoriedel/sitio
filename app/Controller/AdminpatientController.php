<?php
class AdminpatientController extends AppController {

var $uses = array('User','Turn','Turnrule','Doctor','Patient','Day');
public $helpers = array('Html', 'Form', 'UploadPack.Upload');

public function beforeFilter() {
   parent::beforeFilter();
   $this->layout = 'turnos';
    // NOT ALLOW SECRETARY USER TYPE
    if ($this->action != 'login' && $this->controllerUser['usertype_id'] != 3)
    {
        $this->Session->setFlash(__('Acceso restringido'),'session_danger_flash');
        return $this->redirect($this->controllerUser['homeUrl']);
    }
   $this->Auth->allow('login');
}	

public function index() {
	
	/* SI NO EXISTE UN REGISTRO PATIENT ASOCIADO AL USUARIO, LO PATEO AL FIRSTTIME PARA CREARLO */
	$patient = $this->Patient->find('first',array('conditions'=>array('user_id'=>$this->controllerUser['userid'])));
	if(!$patient)
	{
       	$this->Session->setFlash(__('Debe ingresar sus datos para pedir un turno'),'session_warning_flash');
        return $this->redirect(array('controller'=>'Adminpatient','action'=>'firsttime'));
	}
	/* */

    $this->set('os_id',$patient['Patient']['obrasocial_id']);

	/* FILTRAR LOS DOCTORES CON LOS CUALES EL PACIENTE TIENE UN TURNO ACTIVO (EXISTA TURNO DOC=>PAC con FECHATURO > NOW) */    
	$doctors = $this->Doctor->find('all');
	$this->set('doctors',$doctors);	
}

public function firsttime() {
    if ($this->request->is('post')) {
	    $this->Patient->create();
	    if ($this->Patient->save($this->request->data)) {
	        $this->Session->setFlash(__('Sus datos han sido cargados!'),'session_success_flash');
        	$Email = new CakeEmail('gmail');
           	$Email->to($this->request->data['Patient']['email']);
            $Email->template('welcome','welcome');
            $Email->emailFormat('html');
            $Email->viewVars(array( 'username' => $this->request->data['Patient']['firstName'].' '.$this->request->data['Patient']['lastName']));
            $Email->subject('Bienvenido a ICM Olavarria.');
            $Email->send('');


	        return $this->redirect(array('controller'=>'adminpatient','action' => 'index'));
	    }
	    $this->Session->setFlash(__('Oops! sus datos no pudieron ser cargados!'),'session_danger_flash');
	}

$obrasocials = $this->Obrasocial->find('list');
$this->set(compact('obrasocials'));

}

public function login(){
    if ($this->request->is('post')){
    	//var_dump($this->request);

    	$existingUser = $this->User->find('first',array('conditions'=>array('username'=>$this->request['User']['username'])));

		if($this->Auth->login()){
			return $this->redirect(array('controller'=>'Adminpatient','action'=>'index'));
		}	
		else 
		{
	        $this->User->create();
	        if ($this->User->save($this->request->data)){
	        	$this->Auth->login();
	            return $this->redirect(array('controller'=>'Adminpatient','action'=>'firsttime'));
	        }
		}	
    }
}

public function myTurns(){

	
		$id = $this->viewVars['viewUser']['userid'];

		$patient = $this->Patient->find('first',array('conditions'=>array('user_id'=>$id)));
		

		$turns = $this->Turn->find('all',array('conditions'=>array('patient_id'=>$patient['Patient']['id'] )));
		
		$this->set('model',$turns);	

	
}

public function confirmTurn($code){
        
    $this->Turn->updateAll(array('Turn.confirm' => true), 
                            array('Turn.code' => $code));

    $this->Session->setFlash(
            __('El Turno ha sido Confirmado. Lo esperamos!'),'session_success_flash'
        );
        return $this->redirect(array('controller'=>'Adminpatient','action' => 'myTurns'));

}


public function edit() {

    $patient = $this->Patient->find('first',array('conditions'=>array('user_id'=>$this->controllerUser['userid'])));
    $id = $patient['Patient']['id'];
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }
    
    $Patient = $this->Patient->findById($id);
    if (!$Patient) {
        throw new NotFoundException(__('Invalid post'));
    }
    $CurrentUser = $this->User->findById($Patient['Patient']['user_id']);

    if ($this->request->is(array('post', 'put'))) {
        $this->Patient->id = $id;
        
        if ($this->Patient->save($this->request->data)) {
            $this->Session->setFlash(__('Sus datos han sido modificados'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Paciente'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $Patient;
    }

$obrasocials = $this->Obrasocial->find('list');
$this->set(compact('obrasocials'));

}

public function delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Turn->delete($id)) {
        $this->Session->setFlash(
            __('El turno ha sido eliminado'),'session_danger_flash'
        );
        return $this->redirect(array('controller'=>'Adminpatient','action' => 'myTurns'));
    }
}

}

?>