<?php
class PatientsController extends AppController {

public $helpers = array('Html', 'Form');

public function index() {
    $this->set('model',$this->Patient->find('all'));
}

public function add() {
    if ($this->request->is('post')) {
        $this->Patient->create();
        if ($this->Patient->save($this->request->data)) {
            $this->Session->setFlash(__('Paciente ha sido creado!'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops! no se pudo crear Paciente!'),'session_danger_flash');
    }
$users = $this->Patient->User->find('list',array
                                           (
                                               'conditions' => array('usertype_id' => 3)
                                           ));
$this->set(compact('users'));

$obrasocials = $this->Obrasocial->find('list');
$this->set(compact('obrasocials'));

}

public function edit($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $Patient = $this->Patient->findById($id);
    if (!$Patient) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Patient->id = $id;
        if ($this->Patient->save($this->request->data)) {
            $this->Session->setFlash(__('Paciente ha sido modificada'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Paciente'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $Patient;
    }

$users = $this->Patient->User->find('list',array
                                           (
                                               'conditions' => array('usertype_id' => 3)
                                           ));
$this->set(compact('users'));

$obrasocials = $this->Obrasocial->find('list');
$this->set(compact('obrasocials'));

}

public function delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Patient->delete($id)) {
        $this->Session->setFlash(
            __('Paciente ha sido eliminado'),'session_success_flash'
        );
        return $this->redirect(array('action' => 'index'));
    }
}

public function getJsonPatientsByDni(){
    $query = $_GET['term'];

    $result = $this->Patient->find('all', 
        array('conditions' => array('User.username LIKE' => '%'.$query.'%')));
    $this->set('result', $result);
    $this->layout = 'ajax';
}

public function getJsonPatientsByName(){
    $query = $_GET['term'];

    $result = $this->Patient->find('all', 
        array('conditions' => array('Patient.lastname LIKE' => '%'.$query.'%')));
    $this->set('result', $result);
    $this->layout = 'ajax';
}

}
?>
