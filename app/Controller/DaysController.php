<?php
class DaysController extends AppController {

public $helpers = array('Html', 'Form');

public function index() {
    $this->set('model',$this->Day->find('all'));
}

public function add() {
    if ($this->request->is('post')) {
        $this->Day->create();
        if ($this->Day->save($this->request->data)) {
            $this->Session->setFlash(__('Day ha sido creado!'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops! no se pudo crear Day!'),'session_danger_flash');
    }
}

public function edit($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $Day = $this->Day->findById($id);
    if (!$Day) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Day->id = $id;
        if ($this->Day->save($this->request->data)) {
            $this->Session->setFlash(__('Day ha sido modificada'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Day'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $Day;
    }
}

public function delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Day->delete($id)) {
        $this->Session->setFlash(
            __('Day ha sido eliminado'),'session_success_flash'
        );
        return $this->redirect(array('action' => 'index'));
    }
}

}
?>
