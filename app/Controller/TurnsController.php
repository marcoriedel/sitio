<?php
class TurnsController extends AppController {

var $uses = array('Turn','Turnrule','Doctor','Patient','Consultorio','Holiday');
public $helpers = array('Html', 'Form');

public function beforeFilter() {
   parent::beforeFilter();
   $this->Auth->allow('getOpenTurns','getTurnRulesByDoctor','getTurnsDetail','getPatientTurns','confirmTurn','getHolidaysByDoctor');
}   

public function index() {
    $this->set('model',$this->Turn->find('all'));
    
}

public function add() {
    if ($this->request->is('post')) {
        $this->Turn->create();
        if ($this->Turn->save($this->request->data)) {
            $this->Session->setFlash(__('Turn ha sido creado!'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops! no se pudo crear Turn!'),'session_danger_flash');
    }

$doctors = $this->Turn->Doctor->find('list');
$this->set(compact('doctors'));
$patients = $this->Turn->Patient->find('list');
$this->set(compact('patients'));
}

public function edit($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $Turn = $this->Turn->findById($id);
    if (!$Turn) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Turn->id = $id;
        if ($this->Turn->save($this->request->data)) {
            $this->Session->setFlash(__('Turn ha sido modificada'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Turn'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $Turn;
    }
$doctors = $this->Turn->Doctor->find('list');
$this->set(compact('doctors'));
$patients = $this->Turn->Patient->find('list');
$this->set(compact('patients'));
}

public function delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Turn->delete($id)) {
        $this->Session->setFlash(
            __('Turn ha sido eliminado'),'session_success_flash'
        );
        return $this->redirect(array('action' => 'index'));
    }
}

public function getAllTurns($doctor_id,$startDate,$endDate){
     

    $this->layout = 'ajax';
     
     $doctor = $this->Doctor->findById($doctor_id);
     
     if ($doctor)
     {
        $fechaInicio=strtotime($startDate);
        $fechaFin=strtotime($endDate);
        $freeTurns = array();
        
        //Busco las reglas de turno de un doctor
        $doctorTurnRules = $this->getTurnRulesByDoctor($doctor_id);
        
        //Tiempo del turno en segundos segun lo configurado por el doctor
        $turnSeconds = $doctor['Doctor']['turnTime']*60;
        //Divido el lapso de tiempo en rangos segun el tiempo de turno configurado por el Doctor
        //  ese lapso en los turnos ya tomados para agregarlo a los turnos libres.
        for($i=$fechaInicio; $i<=$fechaFin; $i+=$turnSeconds){
            
            //Se le asigna el formato que tienen las reglas de turnos segun day_id
            $turnRuleFormat = date("w H:i:s", $i); 
            
            //Verifico si ese posible turno esta dento de las reglas de turno del Doctor
            if( in_array($turnRuleFormat,$doctorTurnRules)){    

                //Se le da el formato de fecha al posible turno
                $turn = date("Y-m-d H:i:s", $i);     
                    
                   //Se agrega a la lista de todos los turnos
                   array_push($freeTurns, $turn);               
                                   
            }           
        }
        
        return $this->set('model',json_encode($freeTurns));
     }

     $error = array('Mensaje' =>'No existe Doctor');
     return $this->set('model',json_encode($error));  
}


public function getOpenTurns($doctor_id,$startDate,$endDate){
     

    $this->layout = 'ajax';
     
     $doctor = $this->Doctor->findById($doctor_id);
     
     if ($doctor)
     {
        $fechaInicio=strtotime($startDate);
        $fechaFin=strtotime($endDate);
        $freeTurns = array();
        
        //Busco las reglas de turno de un doctor
        $doctorTurnRules = $this->getTurnRulesByDoctor($doctor_id);
        $doctorHolidays = $this->getHolidaysByDoctor($doctor_id);
        
        //Busco los turnos ya tomados del doctor
        $oTurns = $this->Turn->find('all', array(
                                    'conditions' => array('Turn.doctor_id' => $doctor_id)));

        //Armo un array solo con las fechas de los turnos ya tomados
        $turnLinst = array();
        foreach ($oTurns as $key ) {
            array_push($turnLinst, $key['Turn']['datetime']);
        }
        
        $turnSeconds = $doctor['Doctor']['turnTime']*60;
        //Divido el lapso de tiempo en rangos segun el tiempo de turno configurado por el Doctor
        //  ese lapso en los turnos ya tomados para agregarlo a los turnos libres.
        for($i=$fechaInicio; $i<=$fechaFin; $i+=$turnSeconds){
            
            //Se le asigna el formato que tienen las reglas de turnos segun day_id
            $turnRuleFormat = date("w H:i:s", $i); 
            
            //Verifico si ese posible turno esta dento de las reglas de turno del Doctor
            if( in_array($turnRuleFormat,$doctorTurnRules)){    
               

                //Se le da el formato de fecha al posible turno
                $turn = date("Y-m-d H:i:s", $i); 
                    
                   if (! in_array($turn,$doctorHolidays))
                   { 
                    //Si ese posible turno no existe en la lista de turnos
                    //   se agrega a la lista de turnos libres 
                    if (! in_array($turn, $turnLinst)) {array_push($freeTurns, $turn);               
                            }  
                    }                 
            }           
        }
        
        return $this->set('model',json_encode($freeTurns));
     }

     $error = array('Mensaje' =>'No existe Doctor');
     return $this->set('model',json_encode($error));  
}

public function getTurnRulesByDoctor($doctor_id){

    $doctor = $this->Doctor->findById($doctor_id);
     
     if ($doctor)
     {
        $doctorTurnRules = array();


        foreach ($doctor['Turnrule'] as $key ) {
                    array_push($doctorTurnRules,$key);   
        }
        $doctorTimes = array();

        $turnSeconds = $doctor['Doctor']['turnTime']*60;
        foreach ($doctorTurnRules as $key) {
            for($i=strtotime($key['start']); $i<=strtotime($key['end']); $i+=$turnSeconds){
                $turn = $key['day_id'].date(" H:i:s", $i);
                
                array_push($doctorTimes,$turn);
            }
        }
        //var_dump($doctorTimes);
        return $doctorTimes;
     }

}

public function getHolidaysByDoctor($doctor_id){

    $doctor = $this->Doctor->findById($doctor_id);
     //var_dump($doctor);
     if ($doctor)
     {
        $doctorHolidays = array();

        
        foreach ($doctor['Holiday'] as $key ) {
                    array_push($doctorHolidays,$key);   
        }
        $doctorTimes = array();

        $turnSeconds = $doctor['Doctor']['turnTime']*60;
        foreach ($doctorHolidays as $key) {
            if($key['active'])
            {
                for($i=strtotime($key['start']); $i<=strtotime($key['end']); $i+=$turnSeconds){
                    
                    $turn = date("Y-m-d H:i:s", $i); 
                    
                    array_push($doctorTimes,$turn);
                }
            }
        }
        //var_dump($doctorTimes);
        return $doctorTimes;
     }

}

public function getTurnsDetail ($doctor_id){
    $this->layout = 'ajax';

    $doctor = $this->Doctor->findById($doctor_id);
     
     if ($doctor)
     {
        $turns = array();

        foreach ($doctor['Turn'] as $key) {
            
            $patient = $this->Patient->findById($key['patient_id']);
            
            array_push($turns,array('patient'=>$patient['Patient']['name'],
                                    'phone'=>$patient['Patient']['phone'],
                                    'dateTime'=>$key['datetime'],
                                    'turn_id'=>$key['id'],
                                    'confirmed'=>$key['confirm'],
                                    'attended'=>$key['attended']));
        }


        
        return $this->set('model',json_encode($turns));
     }
     $error = array('Mensaje' =>'No existe Doctor');
     return $this->set('model',json_encode($error));  
}

public function getTurnDetail ($turn_id){
    $this->layout = 'ajax';

    $turn = $this->Turn->findById($turn_id);
     
     if ($turn)
     {
        $turns = array();

        $obrasocial = $this->Obrasocial->findById($turn['Patient']['obrasocial_id']);

        $turns['patient'] = $turn['Patient']['name'];
        $turns['address'] = $turn['Patient']['address'];
        $turns['phone'] = $turn['Patient']['phone'];
        $turns['email'] = $turn['Patient']['email'];
        $turns['obrasocial'] = $obrasocial['Obrasocial']['name'];
        $turns['firsttime'] = ($turn['Turn']['first']) ? 'Si' : 'No';

        $this->log($turn,'debug');
        
        return $this->set('model',json_encode($turns));
     }
     $error = array('Mensaje' =>'No existe Doctor');
     return $this->set('model',json_encode($error));  
}


public function getPatientTurns ($patient_id){
    $this->layout = 'ajax';

    $patient = $this->Patient->findById($patient_id);
     //var_dump($patient);
     if ($patient)
     {
        $turns = array();

        foreach ($patient['Turn'] as $key) {
            
            $doctor = $this->Doctor->findById($key['doctor_id']);
            
            array_push($turns,array('Doctor'=>$doctor['Doctor']['name'],
                                    'dateTime'=>$key['datetime'],
                                    'confirmed'=>$key['confirm'],
                                    'attended'=>$key['attended']));
        }


        
        return $this->set('model',json_encode($turns));
     }
     $error = array('Mensaje' =>'No existe patient');
     return $this->set('model',json_encode($error));  
}

public function saveTurn(){
    
    $this->log($this->request,'debug');
    $this->layout = 'ajax';
    $this->Turn->create();
    $this->Turn->set('patient_id',$this->request['data']['patient_id']);
    $this->Turn->set('datetime',$this->request['data']['datetime']);
    $this->Turn->set('doctor_id',$this->request['data']['doctor_id']);
    $this->Turn->set('first',$this->request['data']['cbo_first']);
    $codigo = $this->generarCodigo(20);
    $this->Turn->set('code',$codigo);
    
    if ($this->Turn->save())
    {
        
        $doctor = $this->Doctor->findById($this->request['data']['doctor_id']);
        $patient = $this->Patient->findById($this->request['data']['patient_id']);

        $Email = new CakeEmail('gmail');
        $Email->to($patient['Patient']['email']);
        $Email->template('turn_created','turn_created');
        $Email->emailFormat('html');
        $Email->viewVars(array( 'username' => $patient['Patient']['name'],
                                'doctorName'=> $doctor['Doctor']['name'],
                                'code'=> $codigo,
                                'dateTime'=>$this->request['data']['datetime']));
        $Email->subject('Se ha generado un Nuevo turno.');
        $Email->send('');
        
        $this->set('output','El turno ha sido cargado correctamente!');
    }
    else
    {
        $this->set('output','El turno no se pudo cargar correctamente!');
    }
}

function generarCodigo($longitud) {
     $key = '';
     $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
     $max = strlen($pattern)-1;
     for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
     return $key;
    }

public function confirmTurn($code){
        
    $this->Turn->updateAll(array('Turn.confirm' => true), 
                            array('Turn.code' => $code));

    $this->Session->setFlash(
            __('El Turno ha sido Confirmado. Lo esperamos!'),'session_success_flash'
        );
        return $this->redirect(array('controller'=>'front','action' => 'index'));

}




}

?>
