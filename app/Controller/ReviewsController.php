<?php
class ReviewsController extends AppController {

var $uses = array('Turn','Turnrule','Doctor','Patient','Consultorio','Review');
public $helpers = array('Html', 'Form');

public function beforeFilter() {
   parent::beforeFilter();
   $this->Auth->allow('getOpenTurns','getTurnRulesByDoctor','getTurnsDetail','getPatientTurns','confirmTurn');
}   

public function index() {
    $this->set('model',$this->Review->find('all'));
    
}

public function add() {
    if ($this->request->is('post')) {
        $this->Review->create();
        if ($this->Review->save($this->request->data)) {
            $this->Session->setFlash(__('Review ha sido creado!'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops! no se pudo crear Review!'),'session_danger_flash');
    }

$doctors = $this->Review->Doctor->find('list');
$this->set(compact('doctors'));
$patients = $this->Review->Patient->find('list');
$this->set(compact('patients'));
}

public function edit($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $Review = $this->Review->findById($id);
    if (!$Review) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Review->id = $id;
        if ($this->Review->save($this->request->data)) {
            $this->Session->setFlash(__('Review ha sido modificada'),'session_success_flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Oops, No se pudo modificar Review'),'session_danger_flash');
    }

    if (!$this->request->data) {
        $this->request->data = $Review;
    }
$doctors = $this->Review->Doctor->find('list');
$this->set(compact('doctors'));
$patients = $this->Review->Patient->find('list');
$this->set(compact('patients'));
}

public function delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Review->delete($id)) {
        $this->Session->setFlash(
            __('Review ha sido eliminado'),'session_success_flash'
        );
        return $this->redirect(array('action' => 'index'));
    }
}





}

?>
