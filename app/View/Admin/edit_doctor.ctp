<h1>Editar Doctor</h1>
<div class='container'>
<?php 
echo $this->Form->create('Doctor');
echo $this->Form->input('User.username',array('class'=>'form-control','label'=>'Nombre de usuario(DNI)'));
echo $this->Form->input('User.password',array('class'=>'form-control','label'=>'Contraseña'));
echo '<div style="display:none;">'.$this->Form->input('User.active',array('type'=>'hidden','value'=>true));
echo $this->Form->input('User.usertype_id',array('type'=>'hidden','value'=>4)).'</div>';
echo $this->Form->input('firstName',array('class'=>'form-control','label'=>'Nombre'));
echo $this->Form->input('lastName',array('class'=>'form-control','label'=>'Apellido'));
echo $this->Form->input('email',array('class'=>'form-control','label'=>'Email'));
echo $this->Form->input('registration',array('class'=>'form-control','label'=>'Matricula'));
echo $this->Form->input('specialism',array('class'=>'form-control','label'=>'Especialidad'));
echo $this->Form->input('turnTime',array('class'=>'form-control','label'=>'Minutos por Turno'));
echo $this->Form->input('Doctor.Obrasocial', array('class'=>'radio-inline', 'label'=>'Obras Sociales',  'multiple' => 'checkbox'));
echo $this->Form->input('message',array('class'=>'form-control','label'=>'Aviso para Obra Social no atendida'));
echo $this->Form->end('Guardar');
?>
</div>