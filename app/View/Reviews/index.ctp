<h1>Turn</h1>
<?php echo $this->Html->link('Crear Review',array('controller'=>'Reviews','action'=>'add'),array('class'=>'btn btn-success')); ?>
<br/><br/>


<table class="table table-bordered">
    <tr>
        <th>Id</th>
        <th>Doctor</th>
        <th>Paciente</th>
        <th>Info</th>
        <th>Creado </th>
        <th>Ultima Actualizacion</th>
        <th>Acciones</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->
    
    <?php

    foreach ($model as $Review): ?>
    <tr>
        <td><?php echo $Review['Review']['id']; ?></td>
        <td><?php echo $Review['Doctor']['name']; ?></td>
        <td><?php echo $Review['Patient']['name']; ?></td>
        <td><?php echo $Review['Review']['info']; ?></td>
        <td><?php echo $Review['Review']['created_on']; ?></td>
        <td><?php echo $Review['Review']['updated_on']; ?></td>
        <td>
           <?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-pencil"></span>',
                array('action' => 'edit', $Review['Review']['id']),
                array('escape' => false,'class' => 'btn btn-primary','title' => 'Modificar') // This line will parse rather then output HTML
            ); ?>
                        
            
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($Review); ?>
</table>
