<h1>Nuevo Review</h1>
<div class='container'>
<?php
echo $this->Form->create('Review');
echo $this->Form->input('doctor_id',array('class'=>'form-control','label'=>'Doctor'));
echo $this->Form->input('patient_id',array('class'=>'form-control','label'=>'Paciente'));
echo $this->Form->input('info',array('class'=>'form-control','label'=>'Comentarios'));
echo $this->Form->input('created_on');
echo $this->Form->input('updated_on');
echo $this->Form->end('Guardar');
?>
</div>
