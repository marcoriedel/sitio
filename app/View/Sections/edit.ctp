<h1>Editar Section</h1>
<div class='container'>
<?php
echo $this->Form->create('Section', array('type' => 'file'));
echo $this->Form->input('name',array('class'=>'form-control','label'=>'name'));
echo $this->Form->input('title',array('class'=>'form-control','label'=>'title'));
echo $this->Form->input('slider',array('class'=>'form-control','label'=>'slider'));

echo '<h3>Imagen 1</h3>'.$this->Form->input('Section.image1',array('type'=>'file','label'=>'image1_file_name'));
echo $this->Form->input('image1_title',array('class'=>'form-control','label'=>'Titulo'));
echo $this->Form->input('image1_text',array('class'=>'form-control','label'=>'Texto'));

echo '<h3>Imagen 2</h3>'.$this->Form->input('Section.image2',array('type'=>'file','label'=>'image2_file_name'));
echo $this->Form->input('image2_title',array('class'=>'form-control','label'=>'Titulo'));
echo $this->Form->input('image2_text',array('class'=>'form-control','label'=>'Texto'));

echo '<h3>Imagen 3</h3>'.$this->Form->input('Section.image3',array('type'=>'file','label'=>'image3_file_name'));
echo $this->Form->input('image3_title',array('class'=>'form-control','label'=>'Titulo'));
echo $this->Form->input('image3_text',array('class'=>'form-control','label'=>'Texto'));

echo '<h3>Imagen 4</h3>'.$this->Form->input('Section.image4',array('type'=>'file','label'=>'image4_file_name'));
echo $this->Form->input('image4_title',array('class'=>'form-control','label'=>'Titulo'));
echo $this->Form->input('image4_text',array('class'=>'form-control','label'=>'Texto'));

echo '<h3>Imagen 5</h3>'.$this->Form->input('Section.image5',array('type'=>'file','label'=>'image5_file_name'));
echo $this->Form->input('image5_title',array('class'=>'form-control','label'=>'Titulo'));
echo $this->Form->input('image5_text',array('class'=>'form-control','label'=>'Texto'));

echo $this->Form->end('Guardar');
?>
</div>
