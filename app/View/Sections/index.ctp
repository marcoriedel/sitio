<h1>Section <small>(Uso interno)</small></h1>
<?php echo $this->Html->link('Crear Section',array('controller'=>'Sections','action'=>'add'),array('class'=>'btn btn-success')); ?>
<br/><br/>
<table class="table table-bordered">
    <tr>
        <th>Id</th>
        <th>name</th>
        <th>title</th>
        <th>image1_file_name</th>
        <th>image2_file_name</th>
        <th>image3_file_name</th>
        <th>image4_file_name</th>
        <th>image5_file_name</th>
        <th>Acciones</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->
    
    <?php

    foreach ($model as $Section): ?>
    <tr>
        <td><?php echo $Section['Section']['id']; ?></td>
        <td><?php echo $Section['Section']['name']; ?></td>
        <td><?php echo $Section['Section']['title']; ?></td>
        <?php if ($Section['Section']['slider']): ?>
        <td><?php if ($Section['Section']['image1_file_name']) echo $this->Upload->uploadImage($Section, 'Section.image1', array('style' => 'thumb')); ?></td>
        <td><?php if ($Section['Section']['image2_file_name']) echo $this->Upload->uploadImage($Section, 'Section.image2', array('style' => 'thumb')); ?></td>
        <td><?php if ($Section['Section']['image3_file_name']) echo $this->Upload->uploadImage($Section, 'Section.image3', array('style' => 'thumb')); ?></td>
        <td><?php if ($Section['Section']['image4_file_name']) echo $this->Upload->uploadImage($Section, 'Section.image4', array('style' => 'thumb')); ?></td>
        <td><?php if ($Section['Section']['image5_file_name']) echo $this->Upload->uploadImage($Section, 'Section.image5', array('style' => 'thumb')); ?></td>
        <?php else: ?>
            <td colspan="5"><center>NO TIENE SLIDER</center></td>
        <?php endif; ?>
        <td>
           <?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-pencil"></span>',
                array('action' => 'edit', $Section['Section']['id']),
                array('escape' => false,'class' => 'btn btn-primary','title' => 'Modificar') // This line will parse rather then output HTML
            ); ?>
                        
            <?php
                echo $this->Form->postLink(
                    '<span class="glyphicon glyphicon-remove"></span>',
                    array('action' => 'delete', $Section['Section']['id']),
                    array('confirm' => 'Esta seguro?','escape' => false,'class' => 'btn btn-danger','title' => 'Eliminar')
                );
            ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($Section); ?>
</table>
