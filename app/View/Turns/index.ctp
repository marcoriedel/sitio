<h1>Turn</h1>
<?php echo $this->Html->link('Crear Turn',array('controller'=>'Turns','action'=>'add'),array('class'=>'btn btn-success')); ?>
<br/><br/>


<table class="table table-bordered">
    <thead>
    <tr>
        <th>Id</th>
        <th>Doctor</th>
        <th>Paciente</th>
        <th>Horario</th>
        <th>Confirmado</th>
        <th>Concurrio</th>
        <th>Acciones</th>
    </tr>
    </thead>
    <tbody>

    <!-- Here is where we loop through our $posts array, printing out post info -->
    
    <?php

    foreach ($model as $Turn): ?>
    <tr>
        <td><?php echo $Turn['Turn']['id']; ?></td>
        <td><?php echo $Turn['Doctor']['name']; ?></td>
        <td><?php echo $Turn['Patient']['name']; ?></td>
        <td><?php echo $Turn['Turn']['datetime']; ?></td>
        <td><?php echo $Turn['Turn']['confirm']; ?></td>
        <td><?php echo $Turn['Turn']['attended']; ?></td>
        <td>
           <?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-pencil"></span>',
                array('action' => 'edit', $Turn['Turn']['id']),
                array('escape' => false,'class' => 'btn btn-primary','title' => 'Modificar') // This line will parse rather then output HTML
            ); ?>
                        
            <?php
                echo $this->Form->postLink(
                    '<span class="glyphicon glyphicon-remove"></span>',
                    array('action' => 'delete', $Turn['Turn']['id']),
                    array('confirm' => 'Esta seguro?','escape' => false,'class' => 'btn btn-danger','title' => 'Eliminar')
                );
            ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($Turn); ?>
    </tbody>
</table>
