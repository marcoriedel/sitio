<h1>Editar Turn</h1>
<div class='container'>
<?php
echo $this->Form->create('Turn');
echo $this->Form->input('doctor_id',array('class'=>'form-control','label'=>'Doctor'));
echo $this->Form->input('patient_id',array('class'=>'form-control','label'=>'Paciente'));
echo $this->Form->input('datetime',array('label'=>'Horario'));
echo $this->Form->input('confirm',array('class'=>'form-control','label'=>'Confirmado'));
echo $this->Form->input('attended',array('class'=>'form-control','label'=>'Asistencia'));
echo $this->Form->end('Guardar');
?>
</div>
