<h1>Editar Patient</h1>
<div class='container'>
<?php
echo $this->Form->create('Patient');
echo $this->Form->input('user_id',array('class'=>'form-control','label'=>'user_id'));
echo $this->Form->input('firstName',array('class'=>'form-control','label'=>'firstName'));
echo $this->Form->input('lastName',array('class'=>'form-control','label'=>'lastName'));
echo $this->Form->input('email',array('class'=>'form-control','label'=>'email'));
echo $this->Form->input('obrasocial_id',array('class'=>'form-control','label'=>'Obra Social'));
echo $this->Form->input('socialNumber',array('class'=>'form-control','label'=>'Nro Credencial'));
echo $this->Form->input('address',array('class'=>'form-control','label'=>'Direccion'));
echo $this->Form->end('Guardar');
?>
</div>
