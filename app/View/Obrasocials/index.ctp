<h1>Obrasocial</h1>
<?php echo $this->Html->link('Crear Obrasocial',array('controller'=>'Obrasocials','action'=>'add'),array('class'=>'btn btn-success')); ?>
<br/><br/>
<table class="table table-bordered">
    <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>Acciones</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->
    
    <?php

    foreach ($model as $Obrasocial): ?>
    <tr>
        <td><?php echo $Obrasocial['Obrasocial']['id']; ?></td>
        <td><?php echo $Obrasocial['Obrasocial']['name']; ?></td>
        <td>
           <?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-pencil"></span>',
                array('action' => 'edit', $Obrasocial['Obrasocial']['id']),
                array('escape' => false,'class' => 'btn btn-primary','title' => 'Modificar') // This line will parse rather then output HTML
            ); ?>
                        
            <?php
                echo $this->Form->postLink(
                    '<span class="glyphicon glyphicon-remove"></span>',
                    array('action' => 'delete', $Obrasocial['Obrasocial']['id']),
                    array('confirm' => 'Esta seguro?','escape' => false,'class' => 'btn btn-danger','title' => 'Eliminar')
                );
            ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($Obrasocial); ?>
</table>
