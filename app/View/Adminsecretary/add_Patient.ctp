<h1>Nuevo Paciente</h1>
<div class='container'>
<?php

echo $this->Form->create('User');
echo $this->Form->input('username',array('class'=>'form-control','label'=>'DNI'));
echo $this->Form->input('active',array('class' => 'form-control','type'=>'hidden','label'=>'Activo','value'=>1));
echo $this->Form->input('usertype_id',array('class' => 'form-control','type'=>'hidden','value'=>3));
echo $this->Form->input('password',array('class' => 'form-control','type'=>'hidden','value'=>12345678));

echo $this->Form->create('Patient');
echo $this->Form->input('firstName',array('class'=>'form-control','label'=>'Nombre'));
echo $this->Form->input('lastName',array('class'=>'form-control','label'=>'Apellido'));
echo $this->Form->input('email',array('class'=>'form-control','label'=>'Email'));
echo $this->Form->input('obrasocial_id',array('class'=>'form-control','label'=>'Obra Social'));
echo $this->Form->input('socialNumber',array('class'=>'form-control','label'=>'Nro Credencial'));
echo $this->Form->input('address',array('class'=>'form-control','label'=>'Direccion'));
echo $this->Form->input('phone',array('class'=>'form-control','label'=>'Telefono'));
echo $this->Form->end('Guardar');
?>
</div>
