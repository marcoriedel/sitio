<h1>Listado de Pacientes</h1>
<?php echo $this->Html->link('Crear Nuevo',array('controller'=>'Adminsecretary','action'=>'addPatient'),array('class'=>'btn btn-success')); ?>
<br/><br/>
<table class="table table-bordered">
    <tr>
         <th>Apellido</th>
        <th>Nombre</th>
        <th>DNI</th>
        <th>Telefono</th>
        <th>Email</th>
        <th>Acciones</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->
    
    <?php

    foreach ($model as $Patient): ?>
    <tr>
        <td><?php echo $Patient['Patient']['lastName']; ?></td>
        <td><?php echo $Patient['Patient']['firstName']; ?></td>
        <td><?php echo $Patient['User']['username']; ?></td>
        <td><?php echo $Patient['Patient']['phone']; ?></td>        
        <td><?php echo $Patient['Patient']['email']; ?></td>
        <td>
           <?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-pencil"></span>',
                array('controller'=>'Adminsecretary','action' => 'editPatient', $Patient['Patient']['id']),
                array('escape' => false,'class' => 'btn btn-primary','title' => 'Modificar') // This line will parse rather then output HTML
            ); ?>
            <?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-calendar"></span>',
                array('controller'=>'Adminsecretary','action' => 'turnPatient', $Patient['Patient']['id']),
                array('escape' => false,'class' => 'btn btn-primary','title' => 'Turnos') // This line will parse rather then output HTML
            ); ?>
                        
            
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($Patient); ?>
</table>
