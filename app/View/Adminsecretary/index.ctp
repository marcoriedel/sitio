<style>
.ui-autocomplete { z-index:9999999999999999; }
.fc-event {cursor: pointer;}
.fc-event:hover {background-color: #000 !important}
</style>
<?php echo $this->element('calendar-libs') ?>

<div class="form-group">
<label for="cbo_doctor">1 - Elija doctor:</label>
<select id="cbo_doctor" name="cbo_doctor" class="form-control">
	<?php 
		foreach ($doctors as $key => $value) {
			echo '<option value="'.$value['Doctor']['id'].'">'.$value['Doctor']['firstName'].' '.$value['Doctor']['lastName'].'</option>';
		}
	?>
</select>
</div>

<a class="btn btn-success" id="btnViewSchedule"> Ver agenda</a>
<?php echo $this->Html->link('Crear Paciente',array('controller'=>'adminsecretary','action'=>'addPatient'),array('class'=>'btn btn-primary pull-right')); ?>
<hr/>
<div id="calendar"></div>
<div class="modal fade" id="modalTurn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Crear turno</h4>
      </div>
      <div class="modal-body">
		  <p class="lead">Turno: <span id="lblTurno"></span></p>
		  <p>Buscar paciente
		  <div class="form-group">
		  	<input type="radio" name="radBusqueda" value="dni"/> Por DNI / 
		  	<input type="radio" name="radBusqueda" value="nombre"/> Por Apellido 
		  </div>
		  <div class="form-group ui-front">
			<input type="text" id="txtACPatient" name="txtACPatient" class="form-control" />
		  </div>	  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="btnReservateTurno">Reservar turno</button>
		<button type="button" class="btn btn-primary" onclick="window.location.href='adminsecretary/addPatient'" id="btnCrearPatient">Crear Paciente</button>

      </div>
    </div>
  </div>
</div>
<script>

	var objAux = {};
	var dateTurn,dateTurnFormatted;

	$(function(){

		var saveTurnUrl = '<?php echo $this->html->url(array('controller'=>'turns','action'=>'saveTurn')); ?>';

		$('input[name=radBusqueda]').change(function(){
			if ($(this).val() == 'dni'){
				$('#txtACPatient').autocomplete({
		            source: '<?php echo $this->Html->url(array('controller'=>'Patients', 'action' => 'getJsonPatientsByDni')); ?>', 
					minLength: 2,
		            select: function (event, ui) {
		                objAux = ui.item;
		            }
		        });
			} else {
				$('#txtACPatient').autocomplete({
		            source: '<?php echo $this->Html->url(array('controller'=>'Patients', 'action' => 'getJsonPatientsByName')); ?>', 
					minLength: 2,
		            select: function (event, ui) {
		                objAux = ui.item;
		            }
		        });				
			}
		})

		$('#txtACPatient').autocomplete({
            source: '<?php echo $this->Html->url(array('controller'=>'Patients', 'action' => 'getJsonPatientsByDni')); ?>', 
			minLength: 2,
            select: function (event, ui) {
                objAux = ui.item;
            }
        });

        $('#btnReservateTurno').click(function(){

        	var objTurn = new Object();
			objTurn.patient_id = objAux.patient_id;
			objTurn.datetime = dateTurn;
			objTurn.doctor_id = $('#cbo_doctor').val();
			objTurn.cbo_first = 1; //TODO: INDICAR SI ES FIRST TIME

			var txt;

			swal({
			  title: "Crear turno",
			  text: "¿Desea crear este turno?\nFecha y hora: "+dateTurnFormatted+
						     //"\nDoctor: "+objTurn.doctor_id);
							 "\nDoctor: "+$('#cbo_doctor option:selected').text()+
							 "\nPaciente: "+objAux.name,
			  type: "info",
			  showCancelButton: true,
			  confirmButtonText: "Aceptar",
			  cancelButtonText: "Cancelar",
			  closeOnConfirm: false,
			  closeOnCancel: false
			},
			function(isConfirm){
			  if (isConfirm) {

			    //window.location = "http://localhost/icmweb/adminpatient/myTurns";
			    $.post(saveTurnUrl,objTurn,function(data){
				    swal("Turno creado", "El turno ha sido creado exitosamente", "success");
					$('#calendar').fullCalendar('destroy');
					$('#modalTurn').modal('hide');
					$('#btnViewSchedule').trigger('click');
			    })

			  } else {
			    swal("Operacion cancelada", "El turno NO ha sido creado", "error");
			  }
			});

        });

		var today = new Date();
		var events = [];

		$('#cbo_doctor').change(function(){			
			$('#calendar').fullCalendar('destroy');
			var events = [];
		})

		$('#btnViewSchedule').click(function(){
			$('#calendar').fullCalendar('destroy');

			var start_date = new Date();
			var end_date = new Date();
			end_date.setDate(today.getDate()+90);

			start_date = moment(start_date).format('DD-MM-YYYY');
			end_date = moment(end_date).format('DD-MM-YYYY');

			var events = [];
			var duration = 15;

			var apiUrl = '<?php echo $this->html->url(array('controller'=>'turns','action'=>'getTurnsDetail')); ?>/' + $('#cbo_doctor').val();

			var turnDetailsUrl = '<?php echo $this->html->url(array('controller'=>'turns','action'=>'getTurnDetail')); ?>';
			
			var apiUrlDoctorTime = '<?php echo $this->html->url(array('controller'=>'doctors','action'=>'getTurnsLength')); ?>/' + $('#cbo_doctor').val();

			var apiOpenUrl = '<?php echo $this->html->url(array('controller'=>'turns','action'=>'getOpenTurns')); ?>/'+$('#cbo_doctor').val()+'/'+start_date+'/'+end_date
			
			$.getJSON(apiUrlDoctorTime,function(dataDoctor){
				
				if(dataDoctor){
					duration = dataDoctor;
				}

				$.getJSON(apiUrl,function(data){
					//{"patient":"Marcos Perri","dateTime":"2016-02-01 11:00:00","confirmed":true,"attended":false}
					console.log(data);

					for (var i = 0; i < data.length; i++) {
						var ev = {};
						ev.title = data[i].patient + ' - ' + data[i].phone;
						ev.start = moment(data[i].dateTime);
						ev.turn_id = data[i].turn_id;
						//var m = moment(mockTurns[i].start);
						ev.end = moment(data[i].dateTime).add(duration,'minutes');//m.add(15,'minutes').format('YYYY-MM-DDThh:mm');
						ev.color = (data[i].confirmed) ? '#339933' : '#993333';
						events.push(ev);
					};

					$.getJSON(apiOpenUrl,function(dataOpen){

						/*var parts = dataOpen[i].split(' ');
						var thisday = parts[0];
						var thishout = parts[1];
						var formattedDay = moment(thisday).format("DD/MM/YYYY");*/

						for (var i = 0; i < dataOpen.length; i++) {
							var ev = {};
							ev.title = 'Turno libre';
							ev.start = moment(dataOpen[i]);
							//var m = moment(mockTurns[i].start);
							ev.end = moment(dataOpen[i]).add(duration,'minutes');//m.add(15,'minutes').format('YYYY-MM-DDThh:mm');
							ev.color = '#AAA';
							events.push(ev);
						};

						$('#calendar').fullCalendar({
						slotDuration: '00:'+duration+':00',
						displayEventEnd: {
		                    month: true,
		                    basicWeek: true,
		                    agenda: true,
		                    'default': true
		                },
						defaultDate: moment(today,'dd-MM-yyyy'),
						timeFormat: 'hh:mm', // uppercase H for 24-hour clock
						editable: false,
						header: {
							left: 'prev,next today',
							center: 'title',
							right: 'month,agendaWeek,agendaDay'
						},
						lang: 'es',
						eventLimit: true, // allow "more" link when too many events
						events: events,
						eventClick: function(calEvent, jsEvent, view){
							if(calEvent.title == 'Turno libre'){
								dateTurn = moment(calEvent.start).format('YYYY-MM-DD hh:mm:ss');
								dateTurnFormatted = moment(calEvent.start).format('DD-MM-YYYY h:mm:ss a');
								$('#lblTurno').html(dateTurnFormatted);
								$('#modalTurn').modal('show');
							} else {
								dateTurnFormatted = moment(calEvent.start).format('DD-MM-YYYY h:mm:ss a');
								$.getJSON(turnDetailsUrl+'/'+calEvent.turn_id,function(dataDetails){
									swal({
									  title: "Informacion del turno",
									  text: "Fecha y hora: "+dateTurnFormatted+
												     //"\nDoctor: "+objTurn.doctor_id);
													 "\nPaciente: "+dataDetails.patient+
													 "\nObra social: "+dataDetails.obrasocial+
													 "\nTelefono: "+dataDetails.phone+
													 "\nDireccion: "+dataDetails.address+
													 "\nEmail: "+dataDetails.email+
													 "\nPrimera vez: "+dataDetails.firsttime, 
									  type: "info",
									 });

								});

							}
						}

						});

						$('.fc-today-button').trigger('click');
			
					});

				});
			});

		})

	})
</script>
