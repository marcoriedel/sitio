<div class="row">
	<div class="col-md-6 textdiv">
		<form>
		  <div class="form-group">
		    <label for="cbo_doctor">1 - Elija doctor:</label>
    		<select id="cbo_doctor" name="cbo_doctor" class="form-control">
				<?php 
					foreach ($doctors as $key => $value) {
						echo '<option value="'.$value['Doctor']['id'].'">'.$value['Doctor']['firstName'].' '.$value['Doctor']['lastName'].'</option>';
					}
				?>
			</select>
		  </div>
		  <div class="form-group">
		    <label for="cbo_first">2 - Elija paciente: </label>			
			<select id="cbo_patient" name="cbo_patient" class="form-control">
				<?php 
					foreach ($patients as $key => $value) {
						echo '<option value="'.$value['Patient']['id'].'">'.$value['Patient']['lastName'].' '.$value['Patient']['firstName'].' </option>';
					}
				?>
			</select>
		  </div>
		  <div class="form-group form-inline">
		    <label for="txtSince">3 - Elija rango de fechas:</label>
			<input type="text" id="txtSince" name="txtSince" class="form-control" style="width:100px;" />
			<input type="text" id="txtUntil" name="txtUntil" class="form-control" style="width:100px;"/>
		  </div>
		  <a class="btn btn-success" id="btnSearchTurns">Buscar turnos</a>
		</form>
	</div>
</div>
<br/>
<div class="row">
	<div class="col-md-12 textdiv">
		<div class="row" id="rowTurns">
		</div>
	</div>
</div>


<script type="text/javascript">
	$(function(){

		$('#cbo_doctor').change(function(){
			$('#rowTurns').html('');
		})

		$('#cbo_patient').change(function(){
			$('#rowTurns').html('');
		})

		var weekday = new Array(7);
		weekday[0] =  "domingo";
		weekday[1] = "lunes";
		weekday[2] = "martes";
		weekday[3] = "miercoles";
		weekday[4] = "jueves";
		weekday[5] = "viernes";
		weekday[6] = "s�bado";

		var saveTurnUrl = '<?php echo $this->html->url(array('controller'=>'turns','action'=>'saveTurn')); ?>';

		//Array para dar formato en espa�ol
		 $.datepicker.regional['es'] = 
		 {
		 closeText: 'Cerrar', 
		 prevText: 'Previo', 
		 nextText: 'Pr�ximo',
		 
		 monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		 'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		 monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		 'Jul','Ago','Sep','Oct','Nov','Dic'],
		 monthStatus: 'Ver otro mes', yearStatus: 'Ver otro a�o',
		 dayNames: ['Domingo','Lunes','Martes','Mi�rcoles','Jueves','Viernes','S�bado'],
		 dayNamesShort: ['Dom','Lun','Mar','Mie','Jue','Vie','S�b'],
		 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
		 dateFormat: 'dd/mm/yy', firstDay: 0, 
		 initStatus: 'Selecciona la fecha', isRTL: false};
		$.datepicker.setDefaults($.datepicker.regional['es']);

		var today = new Date();
		var twoWeeks = new Date();
		twoWeeks.setDate(today.getDate() + 14);

		$('#txtSince').datepicker({ defaultDate : today, minDate: "-0D", maxDate: "+1M +10D" });
		$('#txtSince').datepicker('setDate', today);
		$('#txtUntil').datepicker({ defaultDate : twoWeeks, minDate: "+1D", maxDate: "+1M +10D" });
		$('#txtUntil').datepicker('setDate', twoWeeks);

		$('#btnSearchTurns').click(function(){
			$('#rowTurns').html('');
			var patient_id = $('#cbo_patient').val();
			//TODO: Validate fields
			//TODO: Send fields data in order to retrieve intervals
			
			var doctor_id = $('#cbo_doctor').val();
			var start_date = $('#txtSince').val().replace(/\//g,'-');
			var end_date = $('#txtUntil').val().replace(/\//g,'-');
			var cFirst = $('#cbo_first').val();
			

			$.getJSON('turns/getAllTurns/'+doctor_id+'/'+start_date+'/'+end_date,function(data){
				var first = true;
				var currentday = '';
				var html = '';
				for (var i = 0; i < data.length; i++) {
				var parts = data[i].split(' ');
				var thisday = parts[0];
				var thishout = parts[1];
				var formattedDay = moment(thisday).format("DD/MM/YYYY");

				if(first){

					var d = new Date(parts[0]);
					var n = weekday[d.getUTCDay()];

					html += '<div class="col-md-12 well">';
					html += '<h4>Turnos disponibles para el d�a '+ n +' '+formattedDay+'</h4>';
					first = false;
					currentday = thisday
				} else if(currentday != thisday){
					html += '</div>'
					var d = new Date(parts[0]);
					var n = weekday[d.getUTCDay()];

					html += '<div class="col-md-12 well">';
					html += '<h4>Turnos disponibles para el d�a '+ n +' '+formattedDay+'</h4>';
					currentday = thisday
				}

				html += '<div style="display:inline-block;margin:5px;"><input type="hidden" class="txtStartdate" value="'+data[i]+'"/>'+
						'<input type="hidden" class="txtDoctorId" value="'+doctor_id+'"/>'+
						'<input type="hidden" class="txtPatientId" value="'+patient_id+'"/>'+
						'<input type="hidden" class="txtFirst" value="'+cFirst+'"/>'+
						'<a href="#" class="btn btn-primary reservateTurn"><span class="glyphicon glyphicon-calendar"></span> '+parts[1]+'</a></div>'



				}
				html += '</div>';
				$('#rowTurns').append(html);
			});

		});

		$(document).on('click', ".reservateTurn", function () {

			var objTurn = new Object();
			objTurn.patient_id = $(this).parent().find('.txtPatientId').val();
			objTurn.datetime = $(this).parent().find('.txtStartdate').val();
			objTurn.doctor_id = $(this).parent().find('.txtDoctorId').val();
			objTurn.cbo_first = $(this).parent().find('.txtFirst').val();

			var txt;

			swal({
			  title: "Crear turno",
			  text: "�Desea crear este turno?\nFecha y hora: "+objTurn.datetime+
						     //"\nDoctor: "+objTurn.doctor_id);
							 "\nDoctor: "+$('#cbo_doctor option:selected').text()+
							 "\nPaciente: "+$('#cbo_patient option:selected').text(),
			  type: "info",
			  showCancelButton: true,
			  confirmButtonText: "Aceptar",
			  cancelButtonText: "Cancelar",
			  closeOnConfirm: false,
			  closeOnCancel: false
			},
			function(isConfirm){
			  if (isConfirm) {

			    //window.location = "http://localhost/icmweb/adminpatient/myTurns";
			    $.post(saveTurnUrl,objTurn,function(data){
				    swal("Turno creado", "El turno ha sido creado exitosamente", "success");
			    	$('#btnSearchTurns').trigger('click');
			    })

			  } else {
			    swal("Operacion cancelada", "El turno NO ha sido creado", "error");
			  }
			});

        })

	})
</script>










