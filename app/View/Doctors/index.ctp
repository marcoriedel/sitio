<h1>Doctor</h1>
<?php echo $this->Html->link('Crear Doctor',array('controller'=>'Doctors','action'=>'add'),array('class'=>'btn btn-success')); ?>
<br/><br/>
<table class="table table-bordered">
    <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>Matricula</th>
        <th>Especialidad</th>
        <th>Minutos por turno</th>
        <th>Acciones</th>

    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->
    
    <?php

    foreach ($model as $Doctor): ?>
    <tr>
        <td><?php echo $Doctor['Doctor']['id']; ?></td>
        <td><?php echo $Doctor['Doctor']['firstName']." ".$Doctor['Doctor']['lastName']; ?></td>
        <td><?php echo $Doctor['Doctor']['registration']; ?></td>
        <td><?php echo $Doctor['Doctor']['specialism']; ?></td>
        <td><?php echo $Doctor['Doctor']['turnTime']; ?></td>
        <td>
           <?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-pencil"></span>',
                array('action' => 'edit', $Doctor['Doctor']['id']),
                array('escape' => false,'class' => 'btn btn-primary','title' => 'Modificar') // This line will parse rather then output HTML
            ); ?>
                        
            <?php
                echo $this->Form->postLink(
                    '<span class="glyphicon glyphicon-remove"></span>',
                    array('action' => 'delete', $Doctor['Doctor']['id']),
                    array('confirm' => 'Esta seguro?','escape' => false,'class' => 'btn btn-danger','title' => 'Eliminar')
                );
            ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($Doctor); ?>
</table>
