<h1>Editar Datos Generales</h1>
<div class='container'>
<?php
echo $this->Form->create('Consultorio');
echo $this->Form->input('name',array('class'=>'form-control','label'=>'Nombre'));
echo $this->Form->input('address',array('class'=>'form-control','label'=>'Dirección'));
echo $this->Form->input('location',array('class'=>'form-control','label'=>'Ciudad/Provincia/Pais'));
echo $this->Form->input('phone',array('class'=>'form-control','label'=>'Telefono'));
echo $this->Form->input('email',array('class'=>'form-control','label'=>'Email'));
echo $this->Form->end('Guardar');
?>
</div>
