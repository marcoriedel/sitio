<h1>Datos Generales Consultorio</h1>

<br/><br/>
<table class="table table-bordered">
    <tr>
        <th>Nombre</th>
        <th>Dirección</th>
        <th>Ciudad/Provincia/País</th>
        <th>Telefono</th>
        <th>Email</th>
        <th>Acciones</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->
    
    <?php

    foreach ($model as $Consultorio): ?>
    <tr>
        <td><?php echo $Consultorio['Consultorio']['name']; ?></td>
        <td><?php echo $Consultorio['Consultorio']['address']; ?></td>
        <td><?php echo $Consultorio['Consultorio']['location']; ?></td>
        <td><?php echo $Consultorio['Consultorio']['phone']; ?></td>
        <td><?php echo $Consultorio['Consultorio']['email']; ?></td>
        <td>
           <?php echo $this->Html->link(
                'Modificar Datos',
                array('action' => 'edit', $Consultorio['Consultorio']['id']),
                array('escape' => false,'class' => 'btn btn-primary','title' => 'Modificar') // This line will parse rather then output HTML
            ); ?>
                        
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($Consultorio); ?>
</table>
