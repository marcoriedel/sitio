

<h1>Nombre: <?php echo $patient['Patient']['name'] ?> </h1>
<h3>Informacion:</h3>
Obra Social: <?php echo $patient['Obrasocial']['name'] ?><br>
Credencial: <?php echo $patient['Patient']['socialNumber'] ?><br>
DNI: <?php echo $patient['User']['username'] ?>

<h3>Historia Clinica:</h3>

<br/>
<table class="table table-bordered">
    <tr>
        <th>Fecha Creacion</th>
        <th>Informacion</th>
        <th>Acciones</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->
    <?php echo $this->Html->link(
                'Nueva Entrada',
                array('action' => 'addReview',$patient['Patient']['id']),
                array('escape' => false,'class' => 'btn btn-primary')
            ); ?>
    <?php echo $this->Html->link(
                'Terminar Turno',
                array('action' => 'attended', $activeTurn[0]['Turn']['code']),
                array('escape' => false,'class' => 'btn btn-danger') // This line will parse rather then output HTML
            ); ?>
   <br/><br/>
    <?php
    foreach ($reviews as $Review): ?>
    <tr>
        <td><?php echo $Review['Review']['created']; ?></td>
        <td><?php echo $Review['Review']['info']; ?></td>
        <td>
           <?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-pencil"></span>',
                array('action' => 'editReview', $Review['Review']['id']),
                array('escape' => false,'class' => 'btn btn-primary','title' => 'Modificar')
            ); ?>           
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($Review); ?>
</table>




