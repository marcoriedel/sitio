
<div class='container'>

<?php

echo $this->Form->create('Review');
echo $this->Form->input('doctor_id',array('type'=>'hidden','label'=>'Doctor'));
echo $this->Form->input('patient_id',array('type'=>'hidden','label'=>'Paciente'));
echo $this->Form->input('info',array('class'=>'form-control','label'=>'Comentarios'));

echo $this->Form->end('Guardar');
?>

<h3>Historia Clinica:</h3>

<br/>
<table class="table table-bordered">
    <tr>
        <th>Fecha Creacion</th>
        <th>Informacion</th>
    
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->
    
    <?php
    foreach ($reviews as $Review): ?>
    <tr>
        <td><?php echo $Review['Review']['created']; ?></td>
        <td><?php echo $Review['Review']['info']; ?></td>
    
    </tr>
    <?php endforeach; ?>
    <?php unset($Review); ?>
</table>

</div>
