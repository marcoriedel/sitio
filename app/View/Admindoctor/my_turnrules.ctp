<h1>Mis Horarios de Atencion</h1>
<?php echo $this->Html->link('Crear intervalo',array('controller'=>'admindoctor','action'=>'add_turnrule'),array('class'=>'btn btn-success')); ?>
<br/><br/>
<table class="table table-bordered">
    <tr>
        <th>Dia</th>
        <th>Inicio</th>
        <th>Fin</th>
        <th>Acciones</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->
    
    <?php

    foreach ($model as $Turnrule): ?>
    <tr>
        <td><?php echo $Turnrule['Day']['name']; ?></td>
        <td><?php echo $Turnrule['Turnrule']['start']; ?></td>
        <td><?php echo $Turnrule['Turnrule']['end']; ?></td>
        <td>
           <?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-pencil"></span>',
                array('action' => 'edit_turnrule', $Turnrule['Turnrule']['id']),
                array('escape' => false,'class' => 'btn btn-primary','title' => 'Modificar') // This line will parse rather then output HTML
            ); ?>
                        
            <?php
                echo $this->Form->postLink(
                    '<span class="glyphicon glyphicon-remove"></span>',
                    array('action' => 'deleteTurnrule', $Turnrule['Turnrule']['id']),
                    array('confirm' => 'Esta seguro?','escape' => false,'class' => 'btn btn-danger','title' => 'Eliminar')
                );
            ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($Turnrule); ?>
</table>
