<h1>Mis Vacaciones</h1>
<?php echo $this->Html->link('Crear intervalo',array('controller'=>'admindoctor','action'=>'add_holiday'),array('class'=>'btn btn-success')); ?>
<br/><br/>
<table class="table table-bordered">
    <tr>
        
        <th>Inicio</th>
        <th>Fin</th>
        <th>Activo</th>
        <th>Acciones</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->
    
    <?php

    foreach ($model as $Holiday): ?>
    <tr>
        
        <td><?php echo $Holiday['Holiday']['start']; ?></td>
        <td><?php echo $Holiday['Holiday']['end']; ?></td>
        <td>
        <?php  if ($Holiday['Holiday']['active'] == 0) echo "No"; ?>
        <?php  if ($Holiday['Holiday']['active'] == 1) echo "Si"; ?>        
        </td>
        <td>
           <?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-pencil"></span>',
                array('action' => 'edit_holiday', $Holiday['Holiday']['id']),
                array('escape' => false,'class' => 'btn btn-primary','title' => 'Modificar') // This line will parse rather then output HTML
            ); ?>
                        
            <?php
                echo $this->Form->postLink(
                    '<span class="glyphicon glyphicon-remove"></span>',
                    array('action' => 'deleteHoliday', $Holiday['Holiday']['id']),
                    array('confirm' => 'Esta seguro?','escape' => false,'class' => 'btn btn-danger','title' => 'Eliminar')
                );
            ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($Holiday); ?>
</table>
