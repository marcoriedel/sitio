<?php echo $this->element('timepicker-libs'); ?>
<h1>Editar intervalo</h1>
<div class='container'>
<?php
echo $this->Form->create('Holiday');
echo $this->Form->input('doctor_id',array('type'=>'hidden','value'=>$viewUser['doctor_id']));
echo $this->Form->input('start',array('class'=>'form-control','label'=>'Fecha y Hora Inicio'));
echo $this->Form->input('end',array('class'=>'form-control','label'=>'Fecha y Hora Fin'));
echo $this->Form->input('active',array('class'=>'form-control','label'=>'Activo'));
echo $this->Form->end('Guardar');
?>