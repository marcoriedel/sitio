<?php echo $this->element('calendar-libs') ?>
<a class="btn btn-primary" id="btnActiveAddTurn"> Ver Turnos disponibles</a>
<div id="calendar"></div>
<script>

	var apiUrl = '<?php echo $this->html->url(array('controller'=>'turns','action'=>'getTurnsDetail',$viewUser["doctor_id"])); ?>';

	$(function(){
		var today = new Date();

		var events = [];

		//TODO: Ajax call
		$.getJSON(apiUrl,function(data){
			//{"patient":"Marcos Perri","dateTime":"2016-02-01 11:00:00","confirmed":true,"attended":false}
			console.log(data);
			for (var i = 0; i < data.length; i++) {
				var ev = {};
				ev.title = data[i].patient + ' -Tel: ' + data[i].phone;
				ev.start = moment(data[i].dateTime);
				//var m = moment(mockTurns[i].start);
				ev.end = moment(data[i].dateTime).add(15,'minutes');//m.add(15,'minutes').format('YYYY-MM-DDThh:mm');
				ev.color = (data[i].confirmed) ? '#339933' : '#993333';
				events.push(ev);
			};
	
			$('#calendar').fullCalendar({
				slotDuration: '00:15:00',
				defaultDate: moment(today,'dd-MM-yyyy'),
				editable: false,
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay'
				},
				lang: 'es',
				eventLimit: true, // allow "more" link when too many events
				events: events
			});

			$('.fc-today-button').trigger('click');
		});


		

	})
</script>