<table class="table table-bordered">
    <tr>
        <th>Nombre</th>
        <th>Turno</th>
        <th>Acciones</th>
        
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->
    
    <?php foreach ($turns as $Turn): ?>
    <tr>
        <td><?php echo $Turn['Patient']['name']; ?></td>
        <td><?php echo $Turn['Turn']['datetime']; ?></td>
        <td><?php echo $this->Html->link(
                'Iniciar Turno',
                array('action' => 'patientReview', $Turn['Patient']['id']),
                array('escape' => false,'class' => 'btn btn-primary') // This line will parse rather then output HTML
            ); ?>               
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($Turn); ?>
</table>
