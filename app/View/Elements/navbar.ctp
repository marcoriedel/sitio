<nav class='navbar navbar-default'>
  <div class='container-fluid'>
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class='navbar-header'>
      <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1'>
        <span class='sr-only'>Toggle navigation</span>
        <span class='icon-bar'></span>
        <span class='icon-bar'></span>
        <span class='icon-bar'></span>
      </button>
      <?php if ($isAuthenticated): ?>
      <?php 
        $profile_text = '';
        if ($viewUser['usertype_id'] == 2){
          $profile_text = 'Secretaria';
        }
        if ($viewUser['usertype_id'] == 4){
          $profile_text = 'Dr. '.$viewUser['fullname'];
        }
      ?>
      <?php echo $this->Html->link('Administrador - Panel de administracion '.$profile_text,$viewUser['homeUrl'],array('class'=>'navbar-brand','escape'=>false)); ?>
      <?php endif; ?>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>
      <ul class='nav navbar-nav navbar-right'>    
        <?php if ($isAuthenticated): ?>
          <?php if ($viewUser['usertype_id'] == 1): ?>
            <li><?php echo $this->Html->link('Consultorio',array('controller'=>'Consultorios','action'=>'index')); ?></li>
            <li><?php echo $this->Html->link('Secciones',array('controller'=>'Sections','action'=>'index')); ?></li> 
            <li><?php echo $this->Html->link('Articulos',array('controller'=>'Articles','action'=>'index')); ?></li> 
            <li><?php echo $this->Html->link('Usuarios',array('controller'=>'users','action'=>'index')); ?></li>
            <li><?php echo $this->Html->link('Doctores',array('controller'=>'admin','action'=>'doctors')); ?></li>
            <li><?php echo $this->Html->link('Intervalos',array('controller'=>'turnrules','action'=>'index')); ?></li> 
            <li><?php echo $this->Html->link('Days',array('controller'=>'days','action'=>'index')); ?></li> 
            <li><?php echo $this->Html->link('Turns',array('controller'=>'turns','action'=>'index')); ?></li>
            <li><?php echo $this->Html->link('Obras Sociales',array('controller'=>'obrasocials','action'=>'index')); ?></li>
            <li><?php echo $this->Html->link('Pacientes',array('controller'=>'patients','action'=>'index')); ?></li> 
            <li><?php echo $this->Html->link('Reviews',array('controller'=>'reviews','action'=>'index')); ?></li> 
            <li><?php echo $this->Html->link('Vacaciones',array('controller'=>'holidays','action'=>'index')); ?></li> 
          <?php endif;?>
          <?php if ($viewUser['usertype_id'] == 2): ?>
            <li><?php echo $this->Html->link('Agendas',array('controller'=>'adminsecretary','action'=>'index')); ?></li>
            <li><?php echo $this->Html->link('Crear paciente',array('controller'=>'adminsecretary','action'=>'addPatient')); ?></li>
            <li><?php echo $this->Html->link('Pacientes',array('controller'=>'adminsecretary','action'=>'patients')); ?></li>
          <?php endif; ?>
          <?php if ($viewUser['usertype_id'] == 3): ?>
            <li><?php echo $this->Html->link('Editar mis datos',array('controller'=>'adminpatient','action'=>'edit')); ?></li>
            <li><?php echo $this->Html->link('Nuevo Turno',array('controller'=>'adminpatient','action'=>'index')); ?></li>
            <li><?php echo $this->Html->link('Mis Turnos',array('controller'=>'adminpatient','action'=>'myTurns')); ?></li>
          <?php endif; ?>

          <?php if ($viewUser['usertype_id'] == 4): ?>
            <li><?php echo $this->Html->link('Lista de Espera',array('controller'=>'admindoctor','action'=>'waitingList')); ?></li>
            <li><?php echo $this->Html->link('Pacientes',array('controller'=>'admindoctor','action'=>'patients')); ?></li>
            <li><?php echo $this->Html->link('Agenda de turnos',array('controller'=>'admindoctor','action'=>'my_schedule')); ?></li>
            <li><?php echo $this->Html->link('Mis intervalos',array('controller'=>'admindoctor','action'=>'myTurnrules')); ?></li>
            <li><?php echo $this->Html->link('Vacaciones',array('controller'=>'admindoctor','action'=>'myHolidays')); ?></li> 
            <li><?php echo $this->Html->link('Articulos',array('controller'=>'Articles','action'=>'index')); ?></li> 
          <?php endif; ?>
          <li><a><?php echo 'Usuario: '.$viewUser['username']; ?></a></li>
          <li> 
          <?php echo $this->Html->link('Cerrar sesion',array('controller'=>'users','action'=>'logout')); ?></li>
        <?php endif; ?>
        </ul>
    </div>
    
  </div><!-- /.container-fluid -->
</nav>
