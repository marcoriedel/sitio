<?php
$challengeState = $model['Challenge']['state'];
$firstAttr = '';
$secondAttr = '';
$thirdAttr = '';
$fourthAttr = '';

switch($challengeState)
{
    case 1: $firstAttr = 'tr-active';
            $secondAttr = '';
            $thirdAttr = '';
            $fourthAttr = '';
            break;
    case 2: $firstAttr = 'tr-passed';
            $secondAttr = 'tr-active';
            $thirdAttr = '';
            $fourthAttr = '';
            break;
    case 3: $firstAttr = 'tr-passed';
            $secondAttr = 'tr-passed';
            $thirdAttr = 'tr-active';
            $fourthAttr = '';
            break;
    case 4: $firstAttr = 'tr-passed';
            $secondAttr = 'tr-passed';
            $thirdAttr = 'tr-passed';
            $fourthAttr = 'tr-active';
            break;
}
    
?>


<table class="table lefttable">
<tr>
    <th>Periodo</th>
    <th>Fecha limite</th>
</tr>
<tr class="<?php echo $firstAttr ?>">
     <td>Creacion de propuestas</td>
    <td><?php echo $this->Time->format('d/m/Y',$model['Challenge']['deadlineproposal']) ?></td>
</tr>
<tr class="<?php echo $secondAttr ?>">
    <td>Votacion de propuestas</td>
    <td><?php echo $this->Time->format('d/m/Y',$model['Challenge']['deadlinevote']) ?></td>
</tr>
<tr class="<?php echo $thirdAttr ?>">
    <td>Armado del proyecto</td>
    <td><?php echo $this->Time->format('d/m/Y',$model['Challenge']['deadlinefinish']) ?></td>
</tr>
<tr>
    <td>Publicacion</td>
    <td>-</td>
</tr>
</table>