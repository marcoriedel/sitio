<div class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo $this->html->url(array('action'=>'index')) ?>">
            <?php echo $this->html->image('logo.png'); ?>
            </a>
        </div>
        <div class="navbar-collapse collapse ">
            <ul class="nav navbar-nav">
                <li>
                	<?php echo $this->html->link('Inicio',array('controller'=>'front','action'=>'index')); ?>
                </li> 
                <li>
                	<?php echo $this->html->link('Sobre ICM',array('controller'=>'front','action'=>'about')); ?>
                </li> 
                <li>
                	<?php echo $this->html->link('Especialidades',array('controller'=>'front','action'=>'services'));?>
                </li> 
                <li>
                	<?php echo $this->html->link('Articulos',array('controller'=>'front','action'=>'blog')); ?>
                </li> 
                <li>
                	<?php echo $this->html->link('Contacto',array('controller'=>'front','action'=>'contact')); ?>
                </li>
            </ul>
        </div>
    </div>
</div>