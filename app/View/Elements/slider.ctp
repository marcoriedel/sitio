<!-- end header -->
<section id="featured">
 
<!-- Slider -->
    <div id="main-slider" class="flexslider">
        <ul class="slides">
        
          <?php if ($section['Section']['image1_file_name']): ?>
	          <li>
	            <?php echo $this->Upload->uploadImage($section, 'Section.image1', array('style' => 'slide')); ?>
	            <div class="flex-caption">
	                <h3><?php echo strtoupper($section['Section']['image1_title']); ?></h3> 
					<p><?php echo $section['Section']['image1_text']; ?></p>  
	            </div>
	          </li>
      	  <?php endif; ?>
        
          <?php if ($section['Section']['image2_file_name']): ?>
	          <li>
	            <?php echo $this->Upload->uploadImage($section, 'Section.image2', array('style' => 'slide')); ?>
	            <div class="flex-caption">
	                <h3><?php echo strtoupper($section['Section']['image2_title']); ?></h3> 
					<p><?php echo $section['Section']['image2_text']; ?></p>  
	            </div>
	          </li>
      	  <?php endif; ?>
        
          <?php if ($section['Section']['image3_file_name']): ?>
	          <li>
	            <?php echo $this->Upload->uploadImage($section, 'Section.image3', array('style' => 'slide')); ?>
	            <div class="flex-caption">
	                <h3><?php echo strtoupper($section['Section']['image3_title']); ?></h3> 
					<p><?php echo $section['Section']['image3_text']; ?></p>  
	            </div>
	          </li>
      	  <?php endif; ?>
        
          <?php if ($section['Section']['image4_file_name']): ?>
	          <li>
	            <?php echo $this->Upload->uploadImage($section, 'Section.image4', array('style' => 'slide')); ?>
	            <div class="flex-caption">
	                <h3><?php echo strtoupper($section['Section']['image4_title']); ?></h3> 
					<p><?php echo $section['Section']['image4_text']; ?></p>  
	            </div>
	          </li>
      	  <?php endif; ?>
        
          <?php if ($section['Section']['image5_file_name']): ?>
	          <li>
	            <?php echo $this->Upload->uploadImage($section, 'Section.image5', array('style' => 'slide')); ?>
	            <div class="flex-caption">
	                <h3><?php echo strtoupper($section['Section']['image5_title']); ?></h3> 
					<p><?php echo $section['Section']['image5_text']; ?></p>  
	            </div>
	          </li>
      	  <?php endif; ?>
          
        </ul>
    </div>
<!-- end slider -->

</section> 