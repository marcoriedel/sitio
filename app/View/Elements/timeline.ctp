<?php
$challengeState = $model['Challenge']['state'];
$firstAttr = '';
$secondttr = '';
$thirdAttr = '';
$fourthAttr = '';

switch($challengeState)
{
    case 1: $firstAttr = 'active';
            $secondAttr = 'disabled';
            $thirdAttr = 'disabled';
            $fourthAttr = 'disabled';
            break;
    case 2: $firstAttr = 'complete';
            $secondAttr = 'active';
            $thirdAttr = 'disabled';
            $fourthAttr = 'disabled';
            break;
    case 3: $firstAttr = 'complete';
            $secondAttr = 'complete';
            $thirdAttr = 'active';
            $fourthAttr = 'disabled';
            break;
    case 4: $firstAttr = 'complete';
            $secondAttr = 'complete';
            $thirdAttr = 'complete';
            $fourthAttr = 'active';
            break;
}
    
?>
    
<div id="firstStepCol" class="col-xs-3 bs-wizard-step <?php echo $firstAttr ?>">
  <div class="text-center bs-wizard-stepnum">INICIO</div>
  <div class="progress"><div class="progress-bar"></div></div>
  <a href="#" class="bs-wizard-dot"></a>
  <div class="bs-wizard-info text-center">Inicio del desafio.</div>
</div>

<div id="secondStepCol" class="col-xs-3 bs-wizard-step <?php echo $secondAttr ?>"><!-- complete -->
  <div class="text-center bs-wizard-stepnum">VOTACION</div>
  <div class="progress"><div class="progress-bar"></div></div>
  <a href="#" class="bs-wizard-dot"></a>
</div>

<div id="thirdStepCol" class="col-xs-3 bs-wizard-step <?php echo $thirdAttr ?>"><!-- complete -->
  <div class="text-center bs-wizard-stepnum">DECISION</div>
  <div class="progress"><div class="progress-bar"></div></div>
  <a href="#" class="bs-wizard-dot"></a>
</div>

<div id="fourthStepCol" class="col-xs-3 bs-wizard-step <?php echo $fourthAttr ?>"><!-- active -->
  <div class="text-center bs-wizard-stepnum">PROYECTO</div>
  <div class="progress"><div class="progress-bar"></div></div>
  <a href="#" class="bs-wizard-dot"></a>
</div>