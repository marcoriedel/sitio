<?php echo $this->element('timepicker-libs'); ?>
<h1>Editar intervalo</h1>
<div class='container'>
<?php

echo $this->Form->create('Turnrule');
echo $this->Form->input('doctor_id',array('class'=>'form-control','label'=>'Doctor'));
echo $this->Form->input('day_id',array('class'=>'form-control','label'=>'Dia de la semana'));
?>
<div class="form-inline">
	<br/>
	<div class="form-group">	
		<label for="txtSince">Hora de inicio</label>
		<input type="text" class="form-control" id="txtSince"/>
	</div>
	<div class="form-group">	
		<label for="txtUntil">Hora de fin</label>
		<input type="text" class="form-control" id="txtUntil"/>
	</div>
</div>
<?php
echo '<div style="display:none">';
echo $this->Form->input('start',array('label'=>'start'));
echo $this->Form->input('end',array('label'=>'end'));
echo '</div><br/>';
echo $this->Form->end('Guardar');
?>
</div>
<script>
$(function(){
	/*$('#txtSince').timepicker({defaultTime:'09:00 AM'});
	$('#txtUntil').timepicker({defaultTime:'15:00 PM'});
 	$('#TurnruleStartHour').val('09');
 	$('#TurnruleStartMin').val('00');
 	$('#TurnruleStartMeridian').val('am');
 	$('#TurnruleEndHour').val('03');
 	$('#TurnruleEndMin').val('00');
 	$('#TurnruleEndMeridian').val('pm');
*/
	function formatHours(hours){
	 	if (parseInt(hours) < 10)
	 	{ 
	 		return "0" + hours;
	 	}
	 	else 
	 	{
	 		return hours;
	 	}
	}

	function formatMinutes(minutes){
	 	if (parseInt(minutes) < 10)
	 	{ 
	 		return "0" + minutes;
	 	}
	 	else 
	 	{
	 		return minutes;
	 	}
	}

	$('#txtSince').timepicker().on('changeTime.timepicker', function(e) {
	 	$('#TurnruleStartHour').val(formatHours(e.time.hours));
	 	$('#TurnruleStartMin').val(formatMinutes(e.time.minutes));
	 	$('#TurnruleStartMeridian').val(e.time.meridian.toLowerCase());
	  }); 

	 $('#txtUntil').timepicker().on('changeTime.timepicker', function(e) {
	 	$('#TurnruleEndHour').val(formatHours(e.time.hours));
	 	$('#TurnruleEndMin').val(formatMinutes(e.time.minutes));
	 	$('#TurnruleEndMeridian').val(e.time.meridian.toLowerCase());
	  });
})
</script>