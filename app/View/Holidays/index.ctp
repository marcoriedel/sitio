<h1>Intervalos</h1>
<?php echo $this->Html->link('Crear Intervalo',array('controller'=>'Holidays','action'=>'add'),array('class'=>'btn btn-success')); ?>
<br/><br/>
<table class="table table-bordered">
    <tr>
        
        <th>Doctor</th>
        <th>Inicio</th>
        <th>Fin</th>
        <th>Acciones</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->
    
    <?php

    foreach ($model as $Holiday): ?>
    <tr>
        
        <td><?php echo $Holiday['Doctor']['name']; ?></td>
        <td><?php echo $Holiday['Holiday']['start']; ?></td>
        <td><?php echo $Holiday['Holiday']['end']; ?></td>
        <td>
           <?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-pencil"></span>',
                array('action' => 'edit', $Holiday['Holiday']['id']),
                array('escape' => false,'class' => 'btn btn-primary','title' => 'Modificar') // This line will parse rather then output HTML
            ); ?>
                        
            <?php
                echo $this->Form->postLink(
                    '<span class="glyphicon glyphicon-remove"></span>',
                    array('action' => 'delete', $Holiday['Holiday']['id']),
                    array('confirm' => 'Esta seguro?','escape' => false,'class' => 'btn btn-danger','title' => 'Eliminar')
                );
            ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($Holiday); ?>
</table>
