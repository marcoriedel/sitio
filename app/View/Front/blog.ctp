<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="pageTitle">Articulos</h2>
			</div>
		</div>
	</div>
	</section>
	<section id="content">
		<div class="container content">		
        <!-- Service Blcoks -->
        <div class="row service-v1 margin-bottom-40">
            <?php foreach ($articles as $article): ?>
            <div class="col-md-4 md-margin-bottom-40">
               <?php echo $this->Upload->uploadImage($article, 'Article.image', array('style' => 'thumb'), array('class'=>'img-responsive')); ?>
                <h2><?php echo $article['Article']['title']; ?></h2>
                <p><?php echo substr($article['Article']['body'], 0, 200); ?></p>        
                <span class="post-meta-bottom"><a href="<?php echo $this->html->url(array('controller'=>'front','action'=>'article',$article['Article']['id'])) ?>">Ver mas...</a></span>        
            </div>
            <?php endforeach; ?>
        </div>

    </div>
</section>