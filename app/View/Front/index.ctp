	
		
	<section id="content">
	
	
	<div class="container">
		
	     <div class="row">
							<div class="col-md-7">
								<!-- Heading and para -->
								<div class="block-heading-two">
									<h3><span>Sobre ICM</span></h3>
								</div>
								<p>En el año 2002, de un grupo de médicos clínicos de esta ciudad, surgió la idea de formar un centro de atención de clínica médica apuntando a la excelencia académica, científica y cuya principal característica fuera una atención diferente, personalizada y de alta calidad, basada en la tradicional relación del médico con su paciente, tan perdida y desvalorizada actualmente en Centros de Atención de la Salud. Esto último, junto con una capacitación y actualización constante del conocimiento médico son valores basales fundamentales en losque creemos y estamos convencidos.</p>
								<p>De esta manera se forma el Instituto de Clínica Médica (ICM), que en poco tiempo logro ubicarse como unos de los centros de referencia de esta especialidad en la ciudad de Mar del Plata.</p>
								<p>Nuestro objetivo está dirigido hacia una medicina integral en todos sus aspectos. Haciendo hincapié en la prevención y/o diagnóstico temprano de patologías prevalentes, evaluación y tratamiento precoz de factores de riesgo cardiometabólicos, evaluación diagnostica precisa y de calidad, con un enfoque terapéutico basado en una actualización científica constante.</p>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-5">
                 
                    <div class="latest-post-wrap pull-left"> 
						<h3><span>Ultimos Articulos</span></h3>
                        
                        <div class="post-item-wrap pull-left col-sm-6 col-md-12 col-xs-12">
                            <?php echo $this->Upload->uploadImage($article, 'Article.image', array('style' => 'index'), array('class'=>'img-responsive post-author-img')); ?> 
                            	<div class="post-content1 pull-left col-md-9 col-sm-9 col-xs-8">
        	                        <div class="post-title pull-left"><a href="#"><?php echo $article['Article']['title'] ?></a></div>
        	                        <div class="post-meta-top pull-left">
        	                            <ul>
        	                            <li><i class="fa fa-calendar"></i>
        	                            <?php 
        	                            echo $this->Time->format(
										'd/m/Y',
        	                            $article['Article']['created']); ?></li> 
        	                            </ul>
        	                        </div>
                                </div>
                                <div class="post-content2 pull-left">                   
                                	<span class="post-meta-bottom"><a href="<?php echo $this->html->url(array('controller'=>'front','action'=>'article',$article['Article']['id'])) ?>">Ver mas...</a></span></p>
                         		</div>
                         </div>
                         
                  
                        </div>
                    </div>
	
	
							
						</div>
	
	  <div class="row">
		  </div>
	</div>
	
	</section>
	
	
	<section id="service">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block-top">
                            <div class="service-header">
                                <h3>Clinica Medica</h3>
                                <p>La Clínica Médica se ocupa de la atención integral del paciente. Su función principal es guiar al enfermo dentro del sistema hospitalario, dirigiendo la actuación frente a la enfermedad, y coordinando consultas con otros especialistas para lograr el diagnóstico y el tratamiento adecuados.</p>
                            </div>
                        </div>
                    </div><!-- .col-md-12 close -->
                </div><!-- row close -->
<!-- 				<div class="row">
					<div class="col-md-12">
						<div class="block-bottom">
							<div class="service-tab">
								  <!-- Nav tabs --
								  <ul class="badhon-tab" role="tablist">
									<li class=""><a href="#home" aria-controls="home" role="tab" data-toggle="tab">
									<i class="fa fa-ambulance"></i>
									Clinica Medica
									</a></li>
									<li class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
									<i class="fa fa-medkit"></i>
									Nutrición
									</a></li>
									<li><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">
									<i class="fa fa-stethoscope"></i>
									Psicología
									</a></li>
									<li><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">
									<i class="fa fa-heart-o"></i>
									Cardiología
									</a></li> 
								  </ul>

								  <!-- Tab panes --
								  <div class="tab-content edit-tab-content">
									<div class="tab-pane edit-tab" id="home">
										<h3>Clinica Medica</h3>
										<p>Sed ut perspiciaatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur. <br><br>Sed ut perspiciaatis iste natus error sit voluptatem probably haven't heard of them accusamus.</p>
								<p>Voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo quasi architecto beatae vitae dicta sunt explicabo.</p>
									</div>
									<div class="tab-pane edit-tab active" id="profile">
										<h3>Nutrición</h3>
										<p>Sed ut perspiciaatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur. <br><br>Sed ut perspiciaatis iste natus error sit voluptatem probably haven't heard of them accusamus.</p>
								<p>Voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo quasi architecto beatae vitae dicta sunt explicabo.</p>
									</div>
									<div class="tab-pane edit-tab" id="messages">
										<h3>Psicología</h3>
										<p>Sed ut perspiciaatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur. <br><br>Sed ut perspiciaatis iste natus error sit voluptatem probably haven't heard of them accusamus.</p>
								<p>Voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo quasi architecto beatae vitae dicta sunt explicabo.</p>
									</div>
									<div class="tab-pane edit-tab" id="settings">
										<h3>Cardiología</h3>
										<p>Sed ut perspiciaatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur. <br><br>Sed ut perspiciaatis iste natus error sit voluptatem probably haven't heard of them accusamus.</p>
								<p>Voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo quasi architecto beatae vitae dicta sunt explicabo.</p>
									</div> 
								  </div>
							</div>
						</div>
					</div><!-.col-md-12 close -->
				</div><!-- row close --> 
            </div><!-- .container close -->
        </section>
		
<!-- <div class="testimonial-area">
    <div class="testimonial-solid">
        <div class="container"> 
            <div class="row">
                <!--<div class="col-md-12">
                    <div class="block-top">
                        <div class="service-header">
                            <h3>Nuestros auspiciantes</h3>
                        </div>
                    </div>
                </div><!- .col-md-12 close --
            </div><!-- row close --
        	<div class="row" style="text-align:center">
        		<div class="col-md-3">
        			<img src="img/brand.png" alt="brand"/>
        		</div>
        		<div class="col-md-3">
        			<img src="img/brand2.png" alt="brand"/>
        		</div>
        		<div class="col-md-3">
        			<img src="img/brand.png" alt="brand"/>
        		</div>
        		<div class="col-md-3">
        			<img src="img/brand2.png" alt="brand"/>
        		</div>
        	</div>
        </div>
    </div>
</div> -->