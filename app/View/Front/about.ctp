	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="pageTitle">Sobre ICM</h2>
			</div>
		</div>
	</div>
	</section>
	<section id="content">
	<div class="container">
					
					<div class="about">
					
						<div class="row"> 
							<div class="col-md-12">
								<div class="about-logo">
									<h3>Acerca de <span class="color">ICM Olavarría</span></h3>
										<p>En el año 2002, de un grupo de médicos clínicos de esta ciudad, surgió la idea de formar un centro de atención de clínica médica apuntando a la excelencia académica, científica y cuya principal característica fuera una atención diferente, personalizada y de alta calidad, basada en la tradicional relación del médico con su paciente, tan perdida y desvalorizada actualmente en Centros de Atención de la Salud. Esto último, junto con una capacitación y actualización constante del conocimiento médico son valores basales fundamentales en losque creemos y estamos convencidos.</p>
										<p>De esta manera se forma el Instituto de Clínica Médica (ICM), que en poco tiempo logro ubicarse como unos de los centros de referencia de esta especialidad en la ciudad de Mar del Plata.</p>
										<p>Nuestro objetivo está dirigido hacia una medicina integral en todos sus aspectos. Haciendo hincapié en la prevención y/o diagnóstico temprano de patologías prevalentes, evaluación y tratamiento precoz de factores de riesgo cardiometabólicos, evaluación diagnostica precisa y de calidad, con un enfoque terapéutico basado en una actualización científica constante.</p>
								</div>
							</div>
						</div>
						
						<hr>
						<br>
						<!-- Our Team starts -->
				
						<!-- Heading -->
						<div class="block-heading-six">
							<h4 class="bg-color">Nuestro equipo de profesionales</h4>
						</div>
						<br>
						
						<!-- Our team starts -->
						
						<div class="team-six">
							<div class="row">
								<div class="col-md-3 col-sm-6">
									<!-- Team Member -->
									<div class="team-member">
										<!-- Image -->
										<img class="img-responsive" src="img/team1.jpg" alt="">
										<!-- Name -->
										<h4>Dr. Fernando Grimaldi</h4>
										<span class="deg"><strong>Clínica médica</strong></span> 
										<p>MP 91.236 - Especialista consultor en clínica médica, expendido por colegio de médicos de la Provincia de Bs. As.</p>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<!-- Team Member -->
									<div class="team-member">
										<!-- Image -->
										<img class="img-responsive" src="img/team2.jpg" alt="">
										<!-- Name -->
										<h4>Dr. Grimaldi Fernando (hijo)</h4>
										<span class="deg"><strong>Clínica médica</strong></span> 
										<p>MP 447.477 - Especialista Jerarquizado en clínica médica, expendido por el colegio de médicos de la Provincia de Bs. As. </p>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<!-- Team Member -->
									<div class="team-member">
										<!-- Image -->
										<img class="img-responsive" src="img/sanpedro.jpg" alt="">
										<!-- Name -->
										<h4>Dr. Diego San Pedro</h4>
										<span class="deg"><strong>Clínica médica</strong></span> 
										<p>MP 93.413 - Especialista Jerarquizado en clínica médica, expendido por el colegio de médicos de la Provincia de Bs. As.</p>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<!-- Team Member -->
									<div class="team-member">
										<!-- Image -->
										<img class="img-responsive" src="img/alvarez.jpg" alt="">
										<!-- Name -->
										<h4>Dr. Alvarez Federico</h4>
										<span class="deg"><strong>Clínica médica</strong></span> 
										<p>MP 94.792 - Especialista en clínica médica, expendido por el colegio de médicos de la Provincia de Bs. As.</p>     
									</div>
								</div>
							</div>
							<div class="row">	
								<div class="col-md-3 col-sm-6">
									<!-- Team Member -->
									<div class="team-member">
										<!-- Image -->
										<img class="img-responsive" src="img/team4.jpg" alt="">
										<!-- Name -->
										<h4>Lic. Ana Gassó</h4>
										<span class="deg"><strong>Nutrición</strong></span> 
										<p>MP 420 - Licenciada en Nutrición. </p>     
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<!-- Team Member -->
									<div class="team-member">
										<!-- Image -->
										<img class="img-responsive" src="img/team4.jpg" alt="">
										<!-- Name -->
										<h4>Dra Fukiya, Laura</h4>
										<span class="deg"><strong>Clínica Médica</strong></span> 
										<p>Especialista en Clínica Médica, título otorgado por el Colegio de Médico de la Provincia de Buenos Aires </p>     
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<!-- Team Member -->
									<div class="team-member">
										<!-- Image -->
										<img class="img-responsive" src="img/team4.jpg" alt="">
										<!-- Name -->
										<h4>Dr Landetta, Federico</h4>
										<span class="deg"><strong>Cardiología</strong></span> 
										<p>Especialista en Cardiología</p>     
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<!-- Team Member -->
									<div class="team-member">
										<!-- Image -->
										<img class="img-responsive" src="img/team4.jpg" alt="">
										<!-- Name -->
										<h4>Dr Frino, Alejandro</h4>
										<span class="deg"><strong>Neumonología</strong></span> 
										<p>MP 92784 - Especialista en Neumonología, otorgado por Colegio de Médicos de la Pcia de Buenos Aires</p>
									</div>
								</div>
							</div>
							<div class="row">	
								<div class="col-md-3 col-sm-6">
									<!-- Team Member -->
									<div class="team-member">
										<!-- Image -->
										<img class="img-responsive" src="img/team4.jpg" alt="">
										<!-- Name -->
										<h4>Dr Rastelli, Lautaro</h4>
										<span class="deg"><strong>Gastroenterología</strong></span> 
										<p>Especialista en Gastroenterología</p>     
									</div>
								</div>
							</div>
						</div>
						
						<!-- Our team ends -->
					  
						
					</div>
									
				</div>
	</section>