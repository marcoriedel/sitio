<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="pageTitle">Articulos - <?php echo $article['Article']['title'] ?></h2>
            </div>
        </div>
    </div>
    </section>
    <section id="content">
        <div class="container content">     
        <!-- Service Blcoks -->
        <div class="row service-v1 margin-bottom-40">
            <div class="col-md-12 md-margin-bottom-40">
               <?php echo $this->Upload->uploadImage($article, 'Article.image', array('style' => 'page'), array('class'=>'img-responsive')); ?>   
                <h2><?php echo $article['Article']['title'] ?></h2>
                <p><?php echo $article['Article']['body'] ?></p>        
            </div>
        </div>
        
    </div>
    </section>