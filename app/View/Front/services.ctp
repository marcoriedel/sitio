<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="pageTitle">Especialidades</h2>
			</div>
		</div>
	</div>
	</section>
	<section id="content">
		<div class="container content">		
        <!-- Service Blcoks -->
        <div class="row service-v1 margin-bottom-40">
            <div class="col-md-6 md-margin-bottom-40">
               <img class="img-responsive" src="img/slides/1.jpg" alt="">   
                <h2>Clínica Medica</h2>
                <p>La Clínica Médica se ocupa de la atención integral del paciente. Su función principal es guiar al enfermo dentro del sistema hospitalario, dirigiendo la actuación frente a la enfermedad, y coordinando consultas con otros especialistas para lograr el diagnóstico y el tratamiento adecuados.</p>        
            </div>
            <div class="col-md-6 md-margin-bottom-40">
               <img class="img-responsive" src="img/slides/3.jpg" alt="">   
                <h2>Nutrición</h2>
                <p>El servicio tiene como objetivo la difusión de los principios de una buena alimentación y una vida saludable contando con un plan asistencial que se adapta a las necesidades de los pacientes.</p>        
            </div>
        </div>
        
    </div>
    </section>