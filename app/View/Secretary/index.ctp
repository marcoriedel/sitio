<?php echo $this->element('calendar-libs') ?>
1 - Elija doctor: 
<select id="cbo_doctor" name="cbo_doctor">
	<?php 
		foreach ($doctors as $key => $value) {
			echo '<option value="'.$value['Doctor']['id'].'">'.$value['Doctor']['firstName'].' '.$value['Doctor']['lastName'].'</option>';
		}
	?>
</select>
<a class="btn btn-success" id="btnViewSchedule"> Ver agenda</a>
<hr/>
<div id="calendar"></div>
<script>
	$(function(){
		var today = new Date();

		$('#btnViewSchedule').click(function(){

			$('#calendar').fullCalendar('destroy');	

			//TODO: Ajax call
			var mockTurns = [{patient:'Juan Perez', start: '2016-01-09T10:00', confirmed: true},
							 {patient:'Fito Sanchez', start: '2016-01-09T10:15', confirmed: true},
							 {patient:'Pedro Perez', start: '2016-01-09T10:30', confirmed: false},
							 {patient:'Juana Alba', start: '2016-01-09T11:00', confirmed: false},
							 {patient:'Mirta Legrand', start: '2016-01-09T11:30', confirmed: true},
							 {patient:'Pepe Parada', start: '2016-01-09T12:15', confirmed: false},
							 {patient:'Carlos Retamoza', start: '2016-01-09T12:30', confirmed: true},
							 {patient:'Carlos za', start: '2016-01-09T12:45', confirmed: true}];

			var events = [];
			for (var i = 0; i < mockTurns.length; i++) {
				var ev = {};
				ev.title = mockTurns[i].patient;
				ev.start = moment(mockTurns[i].start);
				//var m = moment(mockTurns[i].start);
				ev.end = moment(mockTurns[i].start).add(15,'minutes');//m.add(15,'minutes').format('YYYY-MM-DDThh:mm');
				ev.color = (mockTurns[i].confirmed) ? '#339933' : '#993333';
				console.log(ev);
				events.push(ev);
			};

			$('#calendar').fullCalendar({
				slotDuration: '00:15:00',
				defaultDate: moment(today,'dd-MM-yyy'),
				editable: false,
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay'
				},
				lang: 'es',
				eventLimit: true, // allow "more" link when too many events
				events: events
			});
		

		})

	})
</script>