<h1>Editar Articulo</h1>
<div class='container'>
<?php
echo $this->Form->create('Article', array('type' => 'file'));
echo $this->Form->input('title',array('class'=>'form-control','label'=>'Titulo'));
//el section id prestablecido para la carga de articulos es la seccion de blog
$section_id = $section['Section']['id'];
echo $this->Form->input('section_id',array('class' => 'form-control','type'=>'hidden','label'=>'Seccion','value'=>$section_id));
echo $this->Wysiwyg->input('Article.body', array('style' => 'width: 100%;','label'=>'texto'));
echo $this->Form->input('author',array('class'=>'form-control','label'=>'Autor'));
echo $this->Form->input('Article.image',array('type'=>'file','label'=>'image_file_name'));
echo $this->Form->end('Guardar');
?>
</div>
