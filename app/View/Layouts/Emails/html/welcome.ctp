<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts.Email.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
	<title><?php echo $title_for_layout; ?></title>
</head>
<body>
	<?php echo $this->fetch('content'); ?>
	Hola <?php echo $username; ?> <br>

	Tu usuario ha sido dado de alta correctamente en Clinica ICM Olavarria. 

	<br>
	Recuerda tener siempre tus datos actualizados para una mejor atención.
	<br>


	Puedes ingresar al sitio desde el siguiente 
	<?php echo $this->Html->link('Link',$this->Html->url('/', true)); ?> <br>
	<?php echo $this->Html->link($this->Html->url('/', true)); ?>	


	
</body>
</html>