<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		//echo $this->Html->css('cake.generic');
		echo $this->Html->css('bootstrap');
		echo $this->Html->css('bootstrap-theme');
		echo $this->Html->css('sweetalert');
		echo $this->Html->script('vendor/modernizr-2.6.2-respond-1.1.0.min');
		echo $this->Html->script(array('vendor/jquery-1.11.0.min.js','moment.min.js','sweetalert.min.js'));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');

	?>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600,800' rel='stylesheet' type='text/css'>
</head>
<body>
	<?php echo $this->element('navbar'); ?>
	<div id="wrapper">
		<div class="container">
			<?php echo $this->Session->flash(); ?>			
			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">
			<div class="container">
				&copy; Minicake - 2015
			</div>
		</div>
	</div>
	<?php echo $this->Html->script(array('vendor/bootstrap.min','jquery-ui.min','jquery.dataTables.js','dataTables.bootstrap.js')); ?>

        <!-- bxSlider CSS file -->
        <?php echo $this->Html->css('dataTables.bootstrap.css'); ?>

        <script>
       	$('.table.table-bordered').dataTable( {
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"                
            },
            "pageLength": 25,
            "order": [[ 0, "desc" ]],
        } );
        </script>

        <!-- bxSlider CSS file -->
        <?php echo $this->Html->css(array('jquery-ui.min','jquery-ui.theme.min.css')); ?>
</body>
</html>
