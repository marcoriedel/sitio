<!DOCTYPE html>
<html lang="en">
<head>	
<?php echo $this->Html->charset(); ?>
<title>
	ICM Olavarría - Instituto de clinica medica - <?php echo $section['Section']['title']; ?>
</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="ICM Olavarría | Instituto de Clinica Medica" />
<meta name="author" content="ICM Olavarría" />
<!-- css -->
<?php
	echo $this->Html->meta('icon');

	//echo $this->Html->css('cake.generic');
	echo $this->Html->css(array('bootstrap.min','fancybox/jquery.fancybox','jcarousel','flexslider','owl-carousel/owl.carousel.css','style'));
	echo $this->Html->css('sweetalert');
		echo $this->Html->script(array('vendor/jquery-1.11.0.min.js','moment.min.js','sweetalert.min.js'));

	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');

?> 
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>
<div id="wrapper">
 	
    <div id="callaction" onclick="window.location = 'turnos'">
        <i class="fa fa-calendar fa-3x" style="line-height:1.2"></i>
        SOLICITAR TURNO
    </div>
	<!-- start header -->
	<header>
		<section class="contactInfo">
			<div class="container"> 
		        <div class="col-md-12"> 
		            <div class="contact-area">
		                <ul>
		                    <li><i class="fa fa-plus"></i><strong>Urgencias/Internación - Clinica 25 de Mayo - (0223) 499-4000</strong></li>
		                    <li><i class="fa fa-phone-square"></i><?php echo $consultorio['Consultorio']['phone']; ?></li>
		                    <li><i class="fa fa-envelope-o"></i><?php echo $consultorio['Consultorio']['email']; ?></li>
		                </ul>
		            </div> 
		        </div> 
		    </div>
		</section>	
		<?php echo $this->element('front-navbar'); ?>
	</header>
	<?php echo $this->Session->flash(); ?>
	<?php if($section['Section']['slider']) echo $this->element('slider'); ?>
	<?php echo $this->fetch('content'); ?>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="widget">
						<h5 class="widgetheading"><?php echo $consultorio['Consultorio']['phone']; ?></h5>
						<address>
						<?php echo $consultorio['Consultorio']['address']; ?><br>
					    <?php echo $consultorio['Consultorio']['location']; ?></address>
						<p>
							<i class="icon-phone"></i> <?php echo $consultorio['Consultorio']['phone']; ?><br>
							<i class="icon-envelope-alt"></i> <?php echo $consultorio['Consultorio']['email']; ?>
						</p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="widget">
						<h5 class="widgetheading">Acceso rapido</h5>
						<ul class="link-list">
							<!--<li><a href="#"><strong>SOLICITAR TURNO</strong></a></li>-->
							<li><a href="#">Sobre ICM</a></li>
							<li><a href="#">Especialidades</a></li>
							<li><a href="#">Articulos</a></li>
							<li><a href="#">Contacto</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3">
					<div class="widget">
						<h5 class="widgetheading">Ultimos articulos</h5>
						<ul class="link-list">
							<li><a href="#">Lactancia y Maternidad</a></li>
							<li><a href="#">Niñez y primera infancia</a></li>
							<li><a href="#">Salud en la tercera edad</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3">
					<div class="widget">
						<h5 class="widgetheading">Sobre ICM</h5>
						<p>En el año 2002, de un grupo de médicos clínicos de esta ciudad, surgió la idea de formar un centro de atención de clínica médica apuntando a la excelencia académica, científica y cuya principal característica fuera una atención diferente...</p>
					</div>
				</div>
			</div>
		</div>
		<div id="sub-footer">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="copyright">
							<p>
								<span>&copy; ICM Olavarría 2016 - Todos los derechos reservados </span>
							</p>
						</div>
					</div>
					<div class="col-lg-6">
					</div>
				</div>
			</div>
		</div>
	</footer>
</div>
<?php echo $this->Html->script(array('jquery.js',
'jquery.easing.1.3.js',
'bootstrap.min.js',
'jquery.fancybox.pack.js',
'jquery.fancybox-media.js',
'portfolio/jquery.quicksand.js',
'portfolio/setting.js',
'jquery.flexslider.js',
'animate.js',
'custom.js',
'owl-carousel/owl.carousel.js')); ?>
</body>
</html>
