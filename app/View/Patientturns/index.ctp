1 - Elija doctor: 
<select id="cbo_doctor" name="cbo_doctor">
	<?php 
		foreach ($doctors as $key => $value) {
			echo '<option value="'.$value['Doctor']['id'].'">'.$value['Doctor']['firstName'].' '.$value['Doctor']['lastName'].'</option>';
		}
	?>
</select>

2 - elija rango de fechas
<input type="text" id="txtSince" name="txtSince"/> - 
<input type="text" id="txtUntil" name="txtUntil"/> 
<a class="btn btn-success" id="btnSearchTurns">Buscar turnos</a>
<hr/>
<div>
	<div class="row" id="rowTurns">
	</div>
</div>


<script type="text/javascript">
	$(function(){

		//Array para dar formato en español
		 $.datepicker.regional['es'] = 
		 {
		 closeText: 'Cerrar', 
		 prevText: 'Previo', 
		 nextText: 'Próximo',
		 
		 monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		 'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		 monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		 'Jul','Ago','Sep','Oct','Nov','Dic'],
		 monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
		 dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
		 dayNamesShort: ['Dom','Lun','Mar','Mie','Jue','Vie','Sáb'],
		 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
		 dateFormat: 'dd/mm/yy', firstDay: 0, 
		 initStatus: 'Selecciona la fecha', isRTL: false};
		$.datepicker.setDefaults($.datepicker.regional['es']);

		var today = new Date();
		var twoWeeks = new Date();
		twoWeeks.setDate(today.getDate() + 14);

		$('#txtSince').datepicker({ defaultDate : today, minDate: "-0D", maxDate: "+1M +10D" });
		$('#txtSince').datepicker('setDate', today);
		$('#txtUntil').datepicker({ defaultDate : twoWeeks, minDate: "+1D", maxDate: "+1M +10D" });
		$('#txtUntil').datepicker('setDate', twoWeeks);

		$('#btnSearchTurns').click(function(){
			$('#rowTurns').html('');
			//TODO: Validate fields
			//TODO: Send fields data in order to retrieve intervals

			var mockData = [{'date':'22/01/2015','time':'10:00'},
							 {'date':'22/01/2015','time':'10:15'},
							 {'date':'22/01/2015','time':'10:30'},
							 {'date':'22/01/2015','time':'10:45'},
							 {'date':'22/01/2015','time':'11:00'},
							 {'date':'22/01/2015','time':'11:15'},
							 {'date':'22/01/2015','time':'11:30'}];

			for (var i = 0; i < mockData.length; i++) {
				$('#rowTurns').append('<div class="col-md-3 well">'+
										mockData[i].date+' '+mockData[i].time+
										'<br/><a class="btn btn-primary">Reservar turno</a></div>');
			};

		})

	})
</script>




