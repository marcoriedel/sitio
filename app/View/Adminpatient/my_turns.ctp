<h1>Mis Turnos</h1>

<br/><br/>


<table class="table textdiv">
    <tr>
        <th>Doctor</th>
        
        <th>Horario</th>
        
        <th>Acciones</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->
    
    <?php

    foreach ($model as $Turn): ?>
    
    <?php if ($Turn['Turn']['attended'] != 1): ?>
    <tr class = 'danger'>
    <?php else: ?>
    <tr class = 'success'>
    <?php endif ?>
        <td><?php echo $Turn['Doctor']['name']; ?></td>
        
        <td><?php echo $Turn['Turn']['datetime']; ?></td>
        
        
        <td>
            <?php if ($Turn['Turn']['confirm'] != 1): ?>
           <?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-ok"></span>',
                array('action' => 'confirmTurn', $Turn['Turn']['code']),
                array('escape' => false,'class' => 'btn btn-success','title' => 'Confirmar') 
            ); ?>
            <?php endif;?>         
            <?php
                echo $this->Form->postLink(
                    '<span class="glyphicon glyphicon-remove"></span>',
                    array('action' => 'delete', $Turn['Turn']['id']),
                    array('confirm' => 'Esta seguro?','escape' => false,'class' => 'btn btn-danger','title' => 'Eliminar')
                );
            ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($Turn); ?>
</table>
