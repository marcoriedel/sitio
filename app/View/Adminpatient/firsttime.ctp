
<div class='container'>
<div class="row">
	<div class="col-md-offset-3 col-md-6 textdiv">
		<h3 style="margin-top:0">Ingrese sus datos</h3>
		<hr/>
		<?php
		echo $this->Form->create('Patient');
		echo $this->Form->input('user_id',array('type'=>'hidden','value'=>$viewUser['userid']));
		echo $this->Form->input('firstName',array('class'=>'form-control','label'=>'Nombre'));
		echo $this->Form->input('lastName',array('class'=>'form-control','label'=>'Apellido'));
		echo $this->Form->input('email',array('class'=>'form-control','label'=>'Email','placeholder'=>'Debe ser un mail valido'));
		echo $this->Form->input('obrasocial_id',array('class'=>'form-control','label'=>'Obra Social'));
		echo $this->Form->input('socialNumber',array('class'=>'form-control','label'=>'Nro Credencial'));
		echo $this->Form->input('address',array('class'=>'form-control','label'=>'Direccion'));
		echo $this->Form->input('phone',array('class'=>'form-control','label'=>'Telefono'));
		echo $this->Form->end('Guardar');
		?>
	</div>
</div>

</div>
