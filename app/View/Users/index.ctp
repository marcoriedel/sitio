<h1>User</h1>
<?php echo $this->Html->link('Crear User',array('controller'=>'Users','action'=>'add'),array('class'=>'btn btn-success')); ?>
<br/><br/>
<table class="table table-bordered">
    <tr>
        <th>Id</th>
        <th>DNI</th>
        <th>Activo</th>
        <th>Tipo</th>
        <th>Acciones</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->
    
    <?php

    foreach ($model as $User): ?>
    <tr>
        <td><?php echo $User['User']['id']; ?></td>
        <td><?php echo $User['User']['username']; ?></td>
        <td><?php echo $User['User']['active']; ?></td>
        <td><?php echo $User['Usertype']['name']; ?></td>
        <td>
           <?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-pencil"></span>',
                array('action' => 'edit', $User['User']['id']),
                array('escape' => false,'class' => 'btn btn-primary','title' => 'Modificar') // This line will parse rather then output HTML
            ); ?>
                        
            <?php
                echo $this->Form->postLink(
                    '<span class="glyphicon glyphicon-remove"></span>',
                    array('action' => 'delete', $User['User']['id']),
                    array('confirm' => 'Esta seguro?','escape' => false,'class' => 'btn btn-danger','title' => 'Eliminar')
                );
            ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($User); ?>
</table>
