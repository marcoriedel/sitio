-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 01, 2018 at 02:01 PM
-- Server version: 5.5.51-38.2
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `getitadm_icmolavarria`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL,
  `section_id` int(11) DEFAULT NULL,
  `title` varchar(500) DEFAULT NULL,
  `body` varchar(5000) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `image_file_name` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `section_id`, `title`, `body`, `created`, `modified`, `author`, `image_file_name`) VALUES
(1, 5, 'Articulo de prueba', '<p><span style="color: #656565; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; line-height: 22.4px;">En el a&ntilde;o 2002, de un grupo de m&eacute;dicos cl&iacute;nicos de esta ciudad, surgi&oacute; la idea de formar un centro de atenci&oacute;n de cl&iacute;nica m&eacute;dica apuntando a la excelencia acad&eacute;mica, cient&iacute;fica y cuya principal caracter&iacute;stica fuera una atenci&oacute;n diferente, personalizada y de alta calidad, basada en la tradicional relaci&oacute;n del m&eacute;dico con su paciente. De esta manera se forma el Instituto de Cl&iacute;nica M&eacute;dica (ICM), que en poco tiempo logro ubicase como unos de los centros de referencia de esta especialidad en la ciudad de Mar del Plata.</span></p>', '2016-09-19 00:11:58', '2016-09-30 10:04:45', 'Doctor', '1.jpg'),
(2, 5, 'Articulo 2', '<p style="margin: 0.5em 0px; line-height: inherit; color: #252525; font-family: sans-serif; font-size: 14px;">El concepto de cl&iacute;nica es muy antiguo, sufriendo un proceso evolutivo que ha continuado a lo largo de la historia, recibiendo un importante impulso en su desarrollo inicial con los m&eacute;dicos griegos como&nbsp;<a style="text-decoration: none; color: #0b0080; background: none;" title="Hip&oacute;crates" href="https://es.wikipedia.org/wiki/Hip%C3%B3crates">Hip&oacute;crates</a>&nbsp;en el siglo V antes de Cristo y luego en la&nbsp;<a style="text-decoration: none; color: #0b0080; background: none;" title="Edad Media" href="https://es.wikipedia.org/wiki/Edad_Media">Edad Media</a>&nbsp;y en el Renacimiento, fundamentalmente en los asilos u hoster&iacute;as, despu&eacute;s&nbsp;<a style="text-decoration: none; color: #0b0080; background: none;" title="Hospital" href="https://es.wikipedia.org/wiki/Hospital">hospitales</a>&nbsp;para despose&iacute;dos, enfermos y ancianos abandonados en&nbsp;<a style="text-decoration: none; color: #0b0080; background: none;" title="Pa&iacute;ses Bajos" href="https://es.wikipedia.org/wiki/Pa%C3%ADses_Bajos">Holanda</a>,&nbsp;<a style="text-decoration: none; color: #0b0080; background: none;" title="Francia" href="https://es.wikipedia.org/wiki/Francia">Francia</a>&nbsp;e&nbsp;<a style="text-decoration: none; color: #0b0080; background: none;" title="Italia" href="https://es.wikipedia.org/wiki/Italia">Italia</a>.</p><p style="margin: 0.5em 0px; line-height: inherit; color: #252525; font-family: sans-serif; font-size: 14px;">El referente hist&oacute;rico sobre movimientos de creaci&oacute;n de c&aacute;tedras e institutos cl&iacute;nicos data de los siglos XVII y XVIII en toda Europa, en donde&nbsp;<em>la enfermedad se presenta al observador de acuerdo con s&iacute;ntomas y signos. Los unos y los otros se distinguen por su valor sem&aacute;ntico, as&iacute; como por su morfolog&iacute;a</em>. En esa etapa, la relaci&oacute;n entre el cl&iacute;nico y el enfermo era directa, por lo que las habilidades del explorador, su&nbsp;<a style="text-decoration: none; color: #0b0080; background: none;" title="Inteligencia" href="https://es.wikipedia.org/wiki/Inteligencia">inteligencia</a>, sus destrezas motoras y sensitivas y unos pocos instrumentos, con los que se obten&iacute;an los resultados finales para la elaboraci&oacute;n del diagn&oacute;stico&nbsp;<em>a la par del lecho del enfermo</em>.</p><p style="margin: 0.5em 0px; line-height: inherit; color: #252525; font-family: sans-serif; font-size: 14px;">Esta situaci&oacute;n se mantuvo casi inalterable hasta despu&eacute;s de la Segunda Guerra Mundial, cambio relacionado con el r&aacute;pido desarrollo tecnol&oacute;gico de la segunda mitad del siglo pasado. El desarrollo tecnol&oacute;gico favoreci&oacute; una mayor sensibilidad y especificidad en el diagn&oacute;stico, dando lugar a un gran n&uacute;mero de nuevas enfermedades, &uacute;nicamente identificables gracias a equipos y pruebas de laboratorio sofisticados.</p>', '2016-12-23 11:23:32', '2016-12-23 11:23:32', '', 'software_gestion_clinica_970x420.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `consultorios`
--

CREATE TABLE IF NOT EXISTS `consultorios` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `address` varchar(1000) DEFAULT NULL,
  `phone` varchar(1000) DEFAULT NULL,
  `email` varchar(1000) DEFAULT NULL,
  `location` varchar(500) DEFAULT NULL,
  `confirmationEmailLastSend` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consultorios`
--

INSERT INTO `consultorios` (`id`, `name`, `address`, `phone`, `email`, `location`, `confirmationEmailLastSend`) VALUES
(1, 'ICM Olavarria', 'OlavarrÃ­a 2938 Piso 3Â°C ', '(0223) 451-1385', 'info@icmolavarria.com.ar', 'Mar Del Plata, Buenos Aires, Argentina', '2017-07-10 08:33:02');

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE IF NOT EXISTS `days` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `days`
--

INSERT INTO `days` (`id`, `name`) VALUES
(0, 'Domingo'),
(1, 'Lunes'),
(2, 'Martes'),
(3, 'Miercoles'),
(4, 'Jueves'),
(5, 'Viernes'),
(6, 'Sabado');

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE IF NOT EXISTS `doctors` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `firstName` varchar(256) NOT NULL,
  `lastName` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `registration` varchar(1000) DEFAULT NULL,
  `specialism` varchar(1000) DEFAULT NULL,
  `turnTime` int(11) NOT NULL,
  `message` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`id`, `user_id`, `firstName`, `lastName`, `email`, `registration`, `specialism`, `turnTime`, `message`) VALUES
(8, 85, 'Doctor', 'Prueba', 'marcospucineri@gmail.com', '1234', '1234', 15, 'El doctor no atiende con esa obra social');

-- --------------------------------------------------------

--
-- Table structure for table `doctors_obrasocials`
--

CREATE TABLE IF NOT EXISTS `doctors_obrasocials` (
  `doctor_id` int(11) NOT NULL,
  `obrasocial_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctors_obrasocials`
--

INSERT INTO `doctors_obrasocials` (`doctor_id`, `obrasocial_id`) VALUES
(8, 1),
(8, 2);

-- --------------------------------------------------------

--
-- Table structure for table `obrasocials`
--

CREATE TABLE IF NOT EXISTS `obrasocials` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `obrasocials`
--

INSERT INTO `obrasocials` (`id`, `name`) VALUES
(1, 'OSDE'),
(2, 'Galeno'),
(3, 'Otra'),
(4, 'Swiss Medical');

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE IF NOT EXISTS `patients` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `firstName` varchar(1000) DEFAULT NULL,
  `lastName` varchar(1000) DEFAULT NULL,
  `email` varchar(256) NOT NULL,
  `obrasocial_id` int(11) NOT NULL,
  `socialNumber` int(20) DEFAULT NULL,
  `address` varchar(1000) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`id`, `user_id`, `firstName`, `lastName`, `email`, `obrasocial_id`, `socialNumber`, `address`, `phone`) VALUES
(43, 86, 'Marcos', 'Petterson', 'marcospucineri@gmail.com', 4, 1234, 'Luro 123', '4556611');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `title` varchar(500) DEFAULT NULL,
  `slider` tinyint(1) DEFAULT NULL,
  `image1_file_name` varchar(1000) DEFAULT NULL,
  `image2_file_name` varchar(1000) DEFAULT NULL,
  `image3_file_name` varchar(1000) DEFAULT NULL,
  `image4_file_name` varchar(1000) DEFAULT NULL,
  `image5_file_name` varchar(1000) DEFAULT NULL,
  `image1_title` varchar(100) DEFAULT NULL,
  `image2_title` varchar(100) DEFAULT NULL,
  `image3_title` varchar(100) DEFAULT NULL,
  `image4_title` varchar(100) DEFAULT NULL,
  `image5_title` varchar(100) DEFAULT NULL,
  `image1_text` varchar(300) DEFAULT NULL,
  `image2_text` varchar(300) DEFAULT NULL,
  `image3_text` varchar(300) DEFAULT NULL,
  `image4_text` varchar(300) DEFAULT NULL,
  `image5_text` varchar(300) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `name`, `title`, `slider`, `image1_file_name`, `image2_file_name`, `image3_file_name`, `image4_file_name`, `image5_file_name`, `image1_title`, `image2_title`, `image3_title`, `image4_title`, `image5_title`, `image1_text`, `image2_text`, `image3_text`, `image4_text`, `image5_text`) VALUES
(2, 'index', 'Inicio', 1, 'sld1.jpg', 'sld2.jpg', '2.jpg', NULL, NULL, 'Nuestros consultorios', 'Nuestros especialistas', 'Proximamente turnos online', '', '', 'Equipados para lograr comodidad y lograr una atenciÃ³n adecuada', '', 'En breve, tendra la posibilidad de pedir su turno desde la comodidad de su casa', '', ''),
(3, 'about', 'Sobre ICM', 0, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', ''),
(4, 'services', 'Especialidades', 0, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', ''),
(5, 'blog', 'Articulos', 0, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', ''),
(6, 'contact', 'Contacto', 0, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `turnrules`
--

CREATE TABLE IF NOT EXISTS `turnrules` (
  `id` int(11) NOT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `day_id` int(11) DEFAULT NULL,
  `start` time DEFAULT NULL,
  `end` time DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turnrules`
--

INSERT INTO `turnrules` (`id`, `doctor_id`, `day_id`, `start`, `end`) VALUES
(14, 8, 1, '09:00:00', '15:00:00'),
(15, 8, 3, '09:00:00', '15:00:00'),
(16, 8, 5, '09:00:00', '15:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `turns`
--

CREATE TABLE IF NOT EXISTS `turns` (
  `id` int(11) NOT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `first` tinyint(1) NOT NULL DEFAULT '0',
  `confirm` tinyint(1) DEFAULT NULL,
  `attended` tinyint(1) DEFAULT NULL,
  `code` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turns`
--

INSERT INTO `turns` (`id`, `doctor_id`, `patient_id`, `datetime`, `first`, `confirm`, `attended`, `code`) VALUES
(70, 8, 43, '2017-04-24 10:00:00', 0, NULL, NULL, 'jgttkzjstp3m5t8104ky'),
(71, 8, 43, '2017-07-10 09:00:00', 0, NULL, NULL, '5g31nu1ksat4r90f81jy');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `username` int(8) NOT NULL,
  `password` varchar(256) NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `usertype_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `active`, `usertype_id`) VALUES
(1, 1337, '$2a$10$sjYLVdrdP6rXRkNRblrGe.PVfZeo3fJJUlwNxaCDKt42S81tJ3FKm', 1, 1),
(84, 110, '$2a$10$hQ/cWjeqiSD6LfpKjB/lG.D7B5BQ6rPuiMSRtBBjjVrZW393HOzR6', 1, 2),
(85, 111, '$2a$10$DOUMETk.l60itL5MX.7KOuI0Xb2wLOPGe.WHAUT0F6BOxShyj2l7y', 1, 4),
(86, 123456, '$2a$10$a2r3uZLQCARHPwxy.Gv7uuKOvw5x4ARqg5nFfDyVEO6pp/KKXZ2Lq', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `usertypes`
--

CREATE TABLE IF NOT EXISTS `usertypes` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usertypes`
--

INSERT INTO `usertypes` (`id`, `name`) VALUES
(1, 'Administrador'),
(2, 'Secretaria'),
(3, 'Paciente'),
(4, 'Doctor');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consultorios`
--
ALTER TABLE `consultorios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `days`
--
ALTER TABLE `days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctors_obrasocials`
--
ALTER TABLE `doctors_obrasocials`
  ADD PRIMARY KEY (`doctor_id`,`obrasocial_id`);

--
-- Indexes for table `obrasocials`
--
ALTER TABLE `obrasocials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turnrules`
--
ALTER TABLE `turnrules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turns`
--
ALTER TABLE `turns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usertypes`
--
ALTER TABLE `usertypes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `consultorios`
--
ALTER TABLE `consultorios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `days`
--
ALTER TABLE `days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `obrasocials`
--
ALTER TABLE `obrasocials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `turnrules`
--
ALTER TABLE `turnrules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `turns`
--
ALTER TABLE `turns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `usertypes`
--
ALTER TABLE `usertypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/*Modificaciones 2018*/
ALTER TABLE `patients` CHANGE `obrasocial_id` `obrasocial_id` VARCHAR(256) NULL; 
ALTER TABLE `patients` CHANGE `obrasocial_id` `obrasocial` VARCHAR(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;