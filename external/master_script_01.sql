/* master script 01 */

alter table sections
add title varchar(500) null;

alter table sections
add slider tinyint(1) null;

alter table sections
add image1_file_name varchar(1000) null;

alter table sections
add image2_file_name varchar(1000) null;

alter table sections
add image3_file_name varchar(1000) null;

alter table sections
add image4_file_name varchar(1000) null;

alter table sections
add image5_file_name varchar(1000) null;

/* **************************** */

alter table sections
add image1_title varchar(100) null;

alter table sections
add image2_title varchar(100) null;

alter table sections
add image3_title varchar(100) null;

alter table sections
add image4_title varchar(100) null;

alter table sections
add image5_title varchar(100) null;

alter table sections
add image1_text varchar(300) null;

alter table sections
add image2_text varchar(300) null;

alter table sections
add image3_text varchar(300) null;

alter table sections
add image4_text varchar(300) null;

alter table sections
add image5_text varchar(300) null;

/* Separacion de la dirección y ciudad/prov/pais en Consultorio  */

alter table consultorios 
add location varchar(500) null;

/* Correcciones en tabla Articles */

ALTER TABLE `articles` DROP `text`;

ALTER TABLE `articles` ADD title varchar(500) null;
ALTER TABLE `articles` ADD body varchar(5000) null;
ALTER TABLE `articles` ADD created datetime null;
ALTER TABLE `articles` ADD modified datetime null;
ALTER TABLE `articles` ADD author varchar(100) null;
alter table `articles` ADD image_file_name varchar(1000) null;

/* Correcciones en tabla Articles */
alter table Turnrules
alter column start time null;

alter table Turnrules
alter column end time null;

ALTER TABLE `turns` CHANGE `confirm` `confirm` TINYINT(1) NULL DEFAULT '0';
ALTER TABLE `turns` CHANGE `attended` `attended` TINYINT(1) NULL DEFAULT '0';
ALTER TABLE `turns` ADD `code` VARCHAR(20) NOT NULL ;

/*Modificaciones Septiembre 16*/
ALTER TABLE `patients` ADD `obrasocial_id` INT(11) NOT NULL ;
ALTER TABLE `patients` ADD `socialNumber` INT(20) NULL ;
ALTER TABLE `patients` ADD `address` VARCHAR(1000) NULL ;
ALTER TABLE `turns` ADD `first` BOOLEAN NOT NULL DEFAULT FALSE AFTER `datetime`;
ALTER TABLE `doctors` ADD `turnTime` INT(11) NOT NULL ;
ALTER TABLE `consultorios` ADD `confirmationEmailLastSend` DATE NOT NULL ;

CREATE TABLE IF NOT EXISTS `obrasocials` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `obrasocials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `consultorios` CHANGE `confirmationEmailLastSend` `confirmationEmailLastSend` DATETIME NOT NULL;

CREATE TABLE IF NOT EXISTS `doctors_obrasocials` (
  `doctor_id` int(11) NOT NULL,
  `obrasocial_id` int(11) NOT NULL,
  PRIMARY KEY (`doctor_id`,`obrasocial_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `doctors` ADD `message` VARCHAR(256) NOT NULL AFTER `turnTime`;

ALTER TABLE `patients` ADD `phone` VARCHAR(20) NULL AFTER `address`;

ALTER TABLE `turns` CHANGE `confirm` `confirm` TINYINT(1) NULL DEFAULT '0';
ALTER TABLE `turns` CHANGE `attended` `attended` TINYINT(1) NULL DEFAULT '0';
ALTER TABLE `turns` CHANGE `present` `present` TINYINT(1) NULL DEFAULT '0';
